# Scheduling

## Label based scheduling

Show Nodes and their Labels

```bash
# either with get
$ kubectl get nodes --show-labels

# or describe
$ kubectl describe nodes | grep -A25 -i label
```

Furthermore we can see if those Nodes have any taints:

```bash
$ kubectl describe nodes |grep -i taint
```

The trainer will give one node a label `kind=gpu`.

```bash
$ kubectl label nodes <nodename> kind=gpu
```

The trainer will give one node a label `kind=gpu`.

```bash
$ kubectl label nodes <nodename> kind=memory
```

and verify:

```bash
$ kubectl get nodes --show-labels
```

Now we want to create a pod manifest specifying a `nodeSelector.kind=gpu`:

```bash
cat manifests/nodeselector_gpu.yaml
kubectl apply -f manifests/nodeselector_gpu.yaml
```

Verify if the pod has been spawned on the labeled nodes!

Now create a similar manifest specifiying a `nodeSelector.kind=memory`:
```bash
cat manifests/nodeselector_memory.yaml
kubectl apply -f manifests/nodeselector_memory.yaml
```

Verify.

Cleanup.

```bash
kubectl delete pod gpu-pod memory-pod
```

## Taint based scheduling

Create a new deployment:

```bash
$ kubectl create deployment taint-deployment --replicas=3 --image=quay.io/bitnami/nginx --dry-run=client -oyaml
$ kubectl create deployment taint-deployment --replicas=3 --image=quay.io/bitnami/nginx
```

Verify where the pod has been scheduled

```bash
$ kubectl get po -owide
````

Now we will use a taint. Remember, there are three kind of taints:

* NoSchedule
* PreferNoSchedule
* NoExecute

The trainer will taint a worker node.
```bash
$ kubectl taint nodes <nodename> foo=bar:NoSchedule
````
Verify it has the taint and then create the deployment again. We will use the key of "foo" to illustrate the key name is just some string an admin can use to track pods.

Add a toleration: 
```
kubectl edit deploy taint-deployment
[... pod spec ...]
tolerations:
- key: "foo"
  value: "bar"
  operator: "Equal"
  effect: "NoSchedule"
```


<!---

## Scheduling a Pod without the Scheduler :)

One one of the worker nodes:

Create a the directory /etc/staticpods


```
mkdir /etc/staticpods
```


Create a pod manifest file in this directory


```
apiVersion: v1
kind: Pod
metadata:
  name: staticpod
spec:
  containers:
  - name: staticpod
	image: nginx
```


Configure the kubelet service on this worker node to create pods from /etc/staticpods


```
● kubelet.service - kubelet: The Kubernetes Node Agent
  Loaded: loaded (/lib/systemd/system/kubelet.service; enabled; vendor preset: enabled)
 Drop-In: /etc/systemd/system/kubelet.service.d
          └─10-kubeadm.conf
  Active: active (running) since Mon 2019-04-29 12:07:52 UTC; 2min 50s ago
    Docs: https://kubernetes.io/docs/home/
Main PID: 8099 (kubelet)
   Tasks: 16 (limit: 2320)
  CGroup: /system.slice/kubelet.service
          └─8099 /usr/bin/kubelet --bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes/kubelet.conf --config=/var/lib/kubelet/config.yaml
````

```bash
$ nano /var/lib/kubelet/config.yaml
```

Add the following line

```bash
staticPodPath: /etc/staticpods
```

And restart the kubelet:

```
sudo systemctl daemon-reload
sudo systemctl restart kubelet
```

Now check the cluster:

```bash
$ kubectl get po -owide
```

-->
