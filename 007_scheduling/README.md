# Scheduling

## PodAntiAffinity

We want to run a Deployment and don´t want to run two Pods on the same Node? PodAntiAffinity is your friend.

We have prepared a normal Deployment without AffinityRules. Please have a look in it and apply it to the cluster:
```bash
$ cat manifests/affinity-deployment.yaml
$ kubectl apply -f manifests/affinity-deployment.yaml
```
With the following command you will see, that it is "random", how Pods are distributed. In this example the Pods are distributed equally, but this is not guaranteed:
```
$ kubectl get pods -l app=affinity-deployment -owide
NAME                                   READY   STATUS    RESTARTS   AGE   IP           NODE                          NOMINATED NODE   READINESS GATES
affinity-deployment-7c4c56458f-4qt55   1/1     Running   0          22s   10.0.0.53    aks-aks-55349623-vmss000000   <none>           <none>
affinity-deployment-7c4c56458f-bwc4k   1/1     Running   0          22s   10.0.0.254   aks-aks-55349623-vmss000002   <none>           <none>
affinity-deployment-7c4c56458f-fnjpc   1/1     Running   0          22s   10.0.0.111   aks-aks-55349623-vmss000001   <none>           <none>
```

Now we will add the following PodAntiAffinity:
```yaml
affinity:
  podAntiAffinity:
    requiredDuringSchedulingIgnoredDuringExecution:
    - labelSelector:
        matchExpressions:
        - key: app
          operator: In
          values:
          - affinity-deployment
      topologyKey: failure-domain.beta.kubernetes.io/zone
```
You can apply the manifest file `manifests/affinity-deployment2.yaml`. Now you should see, that the Pods are distributed across all AZs.

Now you are able to see, that one Pod is not schedulable, because we have two Nodes in the same AZ:
```
$ kubectl get pods -l app=affinity-deployment -owide
NAME                                   READY   STATUS    RESTARTS   AGE   IP           NODE                          NOMINATED NODE   READINESS GATES
affinity-deployment-6b84b7c749-bhldk   1/1     Running   0          12s   10.0.0.32    aks-aks-14835901-vmss000000   <none>           <none>
affinity-deployment-6b84b7c749-mx7qm   0/1     Pending   0          12s   <none>       <none>                        <none>           <none>
affinity-deployment-6b84b7c749-nc7xp   1/1     Running   0          12s   10.0.0.123   aks-aks-14835901-vmss000001   <none>           <none>

$ kubectl get nodes -oyaml | grep failure-domain.beta.kubernetes.io/zone
      failure-domain.beta.kubernetes.io/zone: germanywestcentral-1
      failure-domain.beta.kubernetes.io/zone: germanywestcentral-2
      failure-domain.beta.kubernetes.io/zone: germanywestcentral-1
```

## NodeSelector
Now one of the trainers will add a compute-optimized nodePool:
- Node pool name: compute
- Zones: 1,2,3
- Node size: F2s v2
- Optional Settings: 
  - Add label: 
    - compute: true

After a short time, the new Nodes will be added to the Kubernetes cluster. We are able to see them with `kubectl get nodes`. The Nodes will have an additional label `compute: true`, which can be used to assign workload to these specific Node. Another use-case would be a label for GPU-related Nodes, where we can assign our Machine Learning workload to.

To start a Pod on these specific Nodes, we can run the following manifest:
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: compute-pod
  labels:
    type: compute-optimized
spec:
  containers:
  - name: compute1
    image: liquidreply.azurecr.io/busybox:latest
    args:
    - sleep
    - "999999"
  - name: compute2
    image: liquidreply.azurecr.io/busybox:latest
    args:
    - sleep
    - "999999"
  - name: compute3
    image: liquidreply.azurecr.io/busybox:latest
    args:
    - sleep
    - "999999"
  nodeSelector:
    compute: "true"
```