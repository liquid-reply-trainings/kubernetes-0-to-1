
# Kubernetes API Access - Part Two <!-- omit in toc -->

The intention of this chapter is to give you an understanding on what the Kubernetes API is and how to access it. The key here is `kubectl`.
The Kubernetes client, `kubectl` is the primary method of interacting with a Kubernetes cluster. Getting to know it
is essential to using Kubernetes itself.

## Index

- [Accessing the Cluster](#accessing-the-cluster)
  - [`kubectl exec`](#kubectl-exec)
  - [Exercise: Executing Commands within a Remote Pod](#exercise-executing-commands-within-a-remote-pod)
- [Creating Pods](#creating-pods)
  - [Creating a pod via `kubectl`](#creating-a-pod-via-kubectl)
  - [Creating a pod via `curl`](#creating-a-pod-via-curl)
  - [Cleaning up](#cleaning-up)
- [Helpful Resources](#helpful-resources)

---

### `kubectl create`

`kubectl create` creates an object from the commandline (`stdin`) or a supplied json/yaml manifest. The manifests can be
specified with the `-f` or  `--filename` flag that can point to either a file, or a directory containing multiple
manifests.

Command:

```text
kubectl create <type> <parameters>
kubectl create -f <path to manifest>
```

Examples:

```text
$ kubectl create -f manifests/mypod.yaml
pod "mypod" created
```

---

### `kubectl apply`

`kubectl apply` is similar to `kubectl create`. It will essentially update the resource if it is already created, or
simply create it if does not yet exist. When it updates the config, it will save the previous version of it in an
`annotation` on the created object itself. **WARNING:** If the object was not created initially with
`kubectl apply` it's updating behavior will act as a two-way diff. For more information on this, please see the
[kubectl apply](https://kubernetes.io/docs/concepts/cluster-administration/manage-deployment/#kubectl-apply)
documentation.

Just like `kubectl create` it takes a json or yaml manifest with the `-f` flag or accepts input from `stdin`.

Command:

```text
kubectl apply -f <path to manifest>
```

Examples:

```text
$ kubectl apply -f manifests/mypod.yaml
Warning: kubectl apply should be used on resource created by either kubectl create --save-config or kubectl apply
pod "mypod" configured
```

---

### `kubectl edit`

`kubectl edit` modifies a resource in place without having to apply an updated manifest. It fetches a copy of the
desired object and opens it locally with the configured text editor, set by the `KUBE_EDITOR` or `EDITOR` Environment
Variables. This command is useful for troubleshooting, but should be avoided in production scenarios as the changes
will essentially be untracked.

Command:

```text
kubectl edit <type> <object name>
```

Examples:

```text
kubectl edit pod mypod
kubectl edit service myservice
```

---

### `kubectl delete`

`kubectl delete` deletes the object from Kubernetes.

Command:

```text
kubectl delete <type> <name>
```

Examples:

```text
$ kubectl delete pod mypod
pod "mypod" deleted
```

---

### `kubectl logs`

`kubectl logs` outputs the combined `stdout` and `stderr` logs from a pod. If more than one container exist in a
`pod` the `-c` flag is used and the container name must be specified.

Command:

```text
kubectl logs <pod name>
kubectl logs <pod name> -c <container name>
```

Examples:

```text
$ kubectl logs mypod
172.17.0.1 - - [10/Mar/2018:18:14:15 +0000] "GET / HTTP/1.1" 200 612 "-" "curl/7.57.0" "-"
172.17.0.1 - - [10/Mar/2018:18:14:17 +0000] "GET / HTTP/1.1" 200 612 "-" "curl/7.57.0" "-"
```

---

### Exercise: The Basics

**Objective:** Explore the basics. Create a pod and then use the `kubectl` commands to describe and delete
what was created.

**NOTE:** You should still be using the `eks_liquid-training-XXXXXXXX` context created earlier.

---

> Usually the first step would be to create your `dev` namespace.  
> This can be done by using `kubectl create namespace dev-your-name`.  
> In this case we already created a personal namespace just for you!

1. Apply the manifest `manifests/mypod.yaml`.

   ```text
   kubectl apply -f manifests/mypod.yaml
   ```

2. Get the yaml output of the created pod `mypod`.

   ```text
   kubectl get pod mypod -o yaml
   ```

3. Describe the pod `mypod`.

   ```text
   kubectl describe pod mypod
   ```

4. Clean up the pod by deleting it.

   ```text
   kubectl delete pod mypod
   ```

---

**Summary:** The `kubectl` _"CRUD"_ commands are used frequently when interacting with a Kubernetes cluster. These
simple tasks become 2nd nature as more experience is gained.

---

[Back to Index](#index)

---
---

# Accessing the Cluster

`kubectl` provides several mechanisms for accessing resources within the cluster remotely. For this tutorial, the focus will be on using `kubectl exec` to get a remote shell within a container, and `kubectl proxy` to gain access to the services exposed through the API proxy.

---

## `kubectl exec`

`kubectl exec` executes a command within a Pod and can optionally spawn an interactive terminal within a remote
container. When more than one container is present within a Pod, the `-c` or `--container` flag is required, followed
by the container name.

If an interactive session is desired, the `-i` (`--stdin`) and `-t` (`--tty`) flags must be supplied.

Command:

```text
kubectl exec <pod name> -- <arg>
kubectl exec <pod name> -c <container name> -- <arg>
kubectl exec  -i -t <pod name> -c <container name> -- <arg>
kubectl exec  -it <pod name> -c <container name> -- <arg>
```

Example:

```text
$ kubectl exec mypod -c nginx -- printenv
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=mypod
KUBERNETES_SERVICE_PORT_HTTPS=443
KUBERNETES_PORT=tcp://10.96.0.1:443
KUBERNETES_PORT_443_TCP=tcp://10.96.0.1:443
KUBERNETES_PORT_443_TCP_PROTO=tcp
KUBERNETES_PORT_443_TCP_PORT=443
KUBERNETES_PORT_443_TCP_ADDR=10.96.0.1
KUBERNETES_SERVICE_HOST=10.96.0.1
KUBERNETES_SERVICE_PORT=443
NGINX_VERSION=1.12.2
HOME=/root

$ kubectl exec -i -t mypod -c nginx -- /bin/sh
/ #
/ # cat /etc/alpine-release
3.5.2
```

---

## Exercise: Executing Commands within a Remote Pod

**Objective:** Use `kubectl exec` to both initiate commands and spawn an interactive shell within a Pod.

---

1. If not already created, create the Pod `mypod` from the manifest `manifests/mypod.yaml`.

   ```text
   kubectl create -f manifests/mypod.yaml
   ```

2. Wait for the Pod to become ready (`running`).

   ```text
   kubectl get pods --watch
   ```

3. Use `kubectl exec` to `cat` the file `/etc/os-release`.

   ```text
   kubectl exec mypod -- cat /etc/os-release
   ```

   It should output the contents of the `os-release` file.

4. Now use `kubectl exec` and supply the `-i -t` flags to spawn a shell session within the container.

   ```text
   kubectl exec -i -t mypod -- /bin/sh
   ```

   If executed correctly, it should drop you into a new shell session within the nginx container.

5. use `ps aux` to view the current processes within the container.

   ```text
   / # ps aux
   ```

   There should be two nginx processes along with a `/bin/sh` process representing your interactive shell.

6. Exit out of the container simply by typing `exit`.

   With that the shell process will be terminated and the only running processes within the container should once again be nginx and its worker process.

---

**Summary:** `kubectl exec` is not often used, but is an important skill to be familiar with when it comes to Pod
debugging.

---

[Back to Index](#index)

---

# Creating Pods

## Creating a pod via `kubectl`

```bash
cat <<EOF > kuard.yaml
apiVersion: v1
kind: Pod
metadata:
  name: kuard
spec:
  containers:
  - image: gcr.io/kuar-demo/kuard-amd64:blue
    name: kuard
EOF

$ kubectl apply -f kuard.yaml
pod/kuard created

$ kubectl get po kuard
NAME    READY   STATUS    RESTARTS   AGE
kuard   1/1     Running   0          6s
```

## Creating a pod via `curl`

Tip: Use an editor to adjust the namespace inside the queries prior to executing them.

To interact with the Kubernetes API two things are required:

- API Endpoint
- Authorization

As a convenience function, `kubectl proxy` takes over the authentication part...

```bash
$ kubectl proxy &
```

This proxies the Kubernetes API Server on your localhost on port 8001, try it!

```bash
$ curl localhost:8001/api/
```

Output:

```json
{
  "kind": "APIVersions",
  "versions": [
    "v1"
  ],
  "serverAddressByClientCIDRs": [
    {
      "clientCIDR": "0.0.0.0/0",
      "serverAddress": "ip-172-16-53-100.eu-west-1.compute.internal:443"
    }
  ]
}
```

> Try playing with the api!
> 
> `/api/v1/namespaces`  
> `/api/v1/namespaces/<your-namespace`  
> `/api/v1/namespaces/<your-namespace>/pods`  

Now we have all we need to create the Pod. We only need to bring it into the right format:

```bash
$ cat <<EOF > kuard.json
{
  "apiVersion": "v1",
  "kind": "Pod",
  "metadata": {
    "labels": {
      "run": "kuard"
    },
    "name": "kuard"
  },
  "spec": {
    "containers": [
      {
        "image": "gcr.io/kuar-demo/kuard-amd64:blue",
        "name": "kuard"
      }
    ]
  }
}
EOF

Lets send the pod spec to the API server!

$ curl -X POST \
  -H "Content-Type: application/json" \
  127.0.0.1:8001/api/v1/namespaces/<YOUR-NAMESPACE>/pods \
  -d@kuard.json
```

We just did, what `kubectl` usually does for us (minus the authentication) :)


---

## Cleaning up

**NOTE:** If you are proceeding with the next tutorials, simply delete the pod with:

```text
kubectl delete pod mypod
kubectl delete pod kuard
```

The namespace and context will be reused.

---

[Back to Index](#index)

---
---

# Helpful Resources

- [kubectl Overview](https://kubernetes.io/docs/reference/kubectl/overview/)
- [kubectl Cheat Sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
- [kubectl Reference](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands)
- [Accessing Clusters](https://kubernetes.io/docs/tasks/access-application-cluster/access-cluster/)

[Back to Index](#index)

---

This document is based on [https://github.com/mrbobbytables/k8s-intro-tutorials/tree/master/cli](https://github.com/mrbobbytables/k8s-intro-tutorials/tree/master/cli).
