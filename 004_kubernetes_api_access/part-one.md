
# Kubernetes API Access - Part One <!-- omit in toc -->

The intention of this chapter is to give you an understanding on what the Kubernetes API is and how to access it. The key here is `kubectl`.
The Kubernetes client, `kubectl` is the primary method of interacting with a Kubernetes cluster. Getting to know it
is essential to using Kubernetes itself.

## Index

- [Syntax Structure](#syntax-structure)
- [Context and kubeconfig](#context-and-kubeconfig)
  - [`kubectl config`](#kubectl-config)
  - [Exercise: Using Contexts](#exercise-using-contexts)
  - [Kubectl Basics](#kubectl-basics)
    - [`kubectl get`](#kubectl-get)
    - [`kubectl describe`](#kubectl-describe)
    - [`kubectl api-resources`](#kubectl-api-resources)

---

# Syntax Structure

`kubectl` uses a common syntax for all operations in the form of:

```text
kubectl <command> <type> <name> <flags>
```

- **command** - The command or operation to perform. e.g. `apply`, `create`, `delete`, and `get`.
- **type** - The resource type or object.
- **name** - The name of the resource or object.
- **flags** - Optional flags to pass to the command.

Command Examples:

```text
kubectl create -f mypod.yaml
kubectl get pods
kubectl get pod mypod
kubectl delete pod mypod
```

---

[Back to Index](#index)

---

# Context and kubeconfig

`kubectl` allows a user to interact with and manage multiple Kubernetes clusters. To do this, it requires what is known
as a context. A context consists of a combination of `cluster`, `namespace` and `user`.

- **cluster** - A friendly name, server address, and certificate for the Kubernetes cluster.
- **namespace (optional)** - The logical cluster or environment to use. If none is provided, it will use the default `default` namespace.
- **user** - The credentials used to connect to the cluster. This can be a combination of client certificate and key,
username/password, or token.

These contexts are stored in a local YAML based config file referred to as the `kubeconfig`. For \*nix based
systems, the `kubeconfig` is stored in `$HOME/.kube/config`.

This config can be viewed using `kubectl` or with an editor directly.

Command:

```text
kubectl config view
```

Config File Example:

```yaml
$ kubectl config view
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://7403E5B502C13EC0DF481A9DAE183DC5.yl4.eu-west-1.eks.amazonaws.com
  name: eks_liquid-training-T61buzcS
contexts:
- context:
    cluster: eks_liquid-training-T61buzcS
    user: eks_liquid-training-T61buzcS
  name: eks_liquid-training-T61buzcS
current-context: eks_liquid-training-T61buzcS
kind: Config
preferences: {}
users:
- name: eks_liquid-training-T61buzcS
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      args:
      - token
      - -i
      - liquid-training-T61buzcS
      - -r
      - arn:aws:iam::793520626455:role/eks-role-15
      command: aws-iam-authenticator
      env: null
      interactiveMode: IfAvailable
      provideClusterInfo: false
```

---

## `kubectl config`

Managing all aspects of contexts is done via the `kubectl config` command. Some examples include:

- See the active context with `kubectl config current-context`.
- Get a list of available contexts with `kubectl config get-contexts`.
- Switch to using another context with the `kubectl config use-context <context-name>` command.
- Add a new context with `kubectl config set-context <context name> --cluster=<cluster name> --user=<user> --namespace=<namespace>`.

There can be quite a few specifics involved when adding a context, for the available options, please see the
[Configuring Multiple Clusters](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/) Kubernetes documentation.

---

## Exercise: Using Contexts

**Objective:** Switch your current context to your personal namespace.

---

1. View the current contexts.

   ```text
   kubectl config get-contexts
   ```

2. Set your namespace to your personal namespace

   ```text
   kubectl config set-context --current --namespace=<your namespace>
   ```

   ```

3. View the current active context.

   ```text
   kubectl config current-context
   ```

---

**Summary:** Understanding contexts is a base fundamental skill required by every
Kubernetes user. As more clusters and namespaces are added, this can become unwieldy. Installing a helper
application such as [kubectx](https://github.com/ahmetb/kubectx) can be quite helpful. Kubectx allows a user to quickly
switch between contexts and namespaces without having to use the full `kubectl config use-context` command.

---

[Back to Index](#index)

---
---

## Kubectl Basics

There are several `kubectl` commands that are frequently used for any sort of day-to-day operations. `get`, `create`,
`apply`, `delete`, `describe`, and `logs`.  Other commands can be listed simply with `kubectl --help`, or
`kubectl <command> --help`.

---

### `kubectl get`

`kubectl get` fetches and lists objects of a certain type or a specific object itself. It also supports outputting the
information in several different useful formats including: json, yaml, wide (additional columns), or name
(names only) via the `-o` or `--output` flag.

Command:

```text
kubectl get <type>
kubectl get <type> <name>
kubectl get <type> <name> -o <output format>
```

Examples:

```text
$ kubectl get namespaces
NAME          STATUS    AGE
default       Active    4h
kube-public   Active    4h
kube-system   Active    4h

$ kubectl get nodes
NAME                                       STATUS   ROLES    AGE    VERSION
ip-10-0-1-7.eu-west-1.compute.internal     Ready    <none>   176m   v1.21.2-eks-55daa9d
ip-10-0-2-124.eu-west-1.compute.internal   Ready    <none>   178m   v1.21.2-eks-55daa9d
ip-10-0-3-194.eu-west-1.compute.internal   Ready    <none>   3h     v1.21.2-eks-55daa9d

$ kubectl get pod
No resources found in namespace-15 namespace.
```

### `kubectl describe`

Show details of a specific resource or group of resources.

Print a detailed description of the selected resources, including related resources such as events or controllers. You
may select a single object by name, all objects of that type, provide a name prefix, or label selector. For example:

```bash
$ kubectl describe namespace namespace-15
Name:         namespace-15
Labels:       kubernetes.io/metadata.name=namespace-15
Annotations:  <none>
Status:       Active

No resource quota.

No LimitRange resource.

$ kubectl describe nodes kubernetes-node-emt8.c.myproject.internal
[...]
```

---

### `kubectl api-resources`

`kubectl api-resources` prints the supported API resources on the server.