# Organizing Applications

## Index

- [Organizing Applications](#organizing-applications)
  - [Index](#index)
- [Install Helm3](#install-helm3)
- [Creating a new Chart](#creating-a-new-chart)
    - [Installing the Chart](#installing-the-chart)
    - [Validating the Template](#validating-the-template)
    - [Custom-Values](#custom-values)
    - [Customizing the chart](#customizing-the-chart)
      - [Range Loop](#range-loop)
      - [With Statement](#with-statement)
      - [If/Else Statement](#ifelse-statement)
    - [Deleting the Release](#deleting-the-release)
- [Chart Repositories](#chart-repositories)
- [Bonus: customizing a third-party Helm Chart](#bonus-customizing-a-third-party-helm-chart)

---

# Install Helm3

Get Helm from the official GitHub Repository: [https://github.com/helm/helm/releases](https://github.com/helm/helm/releases).

```bash
$ curl -s -L https://get.helm.sh/helm-v3.4.0-linux-amd64.tar.gz |\
tar xz --strip 1 --directory /usr/local/bin linux-amd64/helm

$ helm version
version.BuildInfo{Version:"v3.4.0", GitCommit:"7090a89efc8a18f3d8178bf47d2462450349a004", GitTreeState:"clean", GoVersion:"go1.14.10"}
```

---

[Back to Index](#index)

---

# Creating a new Chart

The following will create a directory called ​myapp​ in the current directory containing several Helm-specificfiles. The example application in the boilerplate chart is a simple Nginx deployment (Nginx is apopular HTTP web server ​https://nginx.org/​).

```bash
$ helm create myapp
$ ls -lah
total 28K
drwxr-xr-x 4 root root 4.0K Nov  7 19:57 .
drwxr-xr-x 3 root root 4.0K Nov  7 19:57 ..
drwxr-xr-x 2 root root 4.0K Nov  7 19:57 charts
-rw-r--r-- 1 root root 1.1K Nov  7 19:57 Chart.yaml
-rw-r--r-- 1 root root  349 Nov  7 19:57 .helmignore
drwxr-xr-x 3 root root 4.0K Nov  7 19:57 templates
-rw-r--r-- 1 root root 1.8K Nov  7 19:57 values.yaml
```

### Installing the Chart

```bash
$ helm install demo myapp
$ helm list
$ kubectl get pods
[...]
```

### Validating the Template

```bash
$ helm template demo .
[a lot of output]
```

If you are brave enough you can also split the output into files:

```bash
$ mkdir output
$ helm template demo . | awk -vout=output -F": " '$0~/^# Source: /{file=out"/"$2; print "Creating "file; system ("mkdir -p $(dirname "file"); echo -n "" > "file)} $0!~/^#/ && $0!="---"{print $0 >> file}'
```

[Source](https://github.com/helm/helm/issues/4680#issuecomment-613201032) of the "handy" one-liner.

### Custom-Values

The values.yaml can and should be overwritten. It is not recommend to adjust values.yaml directly. A best-practice approach is to create a separate file containing values of the helm chart that need adjustment.

The following custom configuration will edit the repository and the tag of the image used:

```bash
$ cat > custom-values.yaml <<EOF
image:
  repository: bitnami.com/nginx
  tag: "1.19"
```

Now we can instruct helm to honor this file:

```bash
$ helm template . -f ./custom-values.yaml
```

Single values could also be overwritten in-line:

```bash
helm template . -f ./custom-values.yaml --set image.tag="1.18"
```

### Customizing the chart
Our helm-chart is able to deploy a few resources that are automatically added when executing `helm create`. You can have a look for these resources in the `template` directory. 
```bash
$ ls templates/
```
Resources that are created:
- Deployment
- Service
- ServiceAccount
- Ingress
- HPA

For most use-cases these resources are sufficient to deploy an application to your Kubernetes cluster. 

#### Range Loop
Lets assume you are deploying an application which needs some ports to be exposed. If we have a look in the `deployment.yaml` and the `service.yaml`, this is not supported out-of-the-box. We are going to change the `deplyoment.yaml` to expose a list of ports. To do that, we will add the following block to our `custom-values.yaml`:
```yaml
deployment:
  ports:
    - name: http
      containerPort: 80
      protocol: TCP
    - name: metrics
      containerPort: 9000
      protocol: TCP
```
In order to add these values to the `deployment.yaml`, we will use a [range loop](https://helm.sh/docs/chart_template_guide/control_structures/#looping-with-the-range-action).
Please try yourself to implement the code, before having a look in the solution :). You are able to validate your changes with `helm template`.
<details>
  <summary>Solution  (<i>click to expand</i>)</summary>
  <!-- have to be followed by an empty line! -->

```yaml
ports:  
{{ range .Values.deployment.ports }}
  - name: {{ .name }}
    containerPort: {{ .containerPort }}
    protocol: {{ .protocol }}
{{ end }}
```  
</details>

#### With Statement
Another flow control is the `with` statement. It can check if a key was passed to the values and insert the value into our templates. If the key was not passed, it is possible to skip it. You can find the official documentation on [helm.sh](https://helm.sh/docs/chart_template_guide/control_structures/#modifying-scope-using-with). We want to add an additional ConfigMap to our application, that should look like this:
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: <name of your helm-chart>
data:
  player_initial_lives: "3"
  ui_properties_file_name: "user-interface.properties"
```
We will add the following block to our values.yaml:
```yaml
plugin:
  player_initial_lives: "3"
  ui_properties_file_name: "user-interface.properties"
```
Please try yourself to implement the code, before having a look in the solution :). You are able to validate your changes with `helm template`.
<details>
  <summary>Solution  (<i>click to expand</i>)</summary>
  <!-- have to be followed by an empty line! -->

```yaml
{{- with .Values.plugin }}
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ $.Release.Name }}
spec:
  player_initial_lives: {{ .player_initial_lives }}
  ui_properties_file_name: {{ .ui_properties_file_name }}
{{- end }}
```  
</details>

#### If/Else Statement
The more elegant way to solve this special use-case is a [If/Else condition](https://helm.sh/docs/chart_template_guide/control_structures/#ifelse). Try to adapt the following values:
```yaml
plugin:  
  enabled: true
  values:
    player_initial_lives: "3"
    ui_properties_file_name: "user-interface.properties"
```
Please try yourself to implement the code, before having a look in the solution :). You are able to validate your changes with `helm template`.
<details>
  <summary>Solution  (<i>click to expand</i>)</summary>
  <!-- have to be followed by an empty line! -->

```yaml
{{- if .Values.plugin.enabled }}
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "test.fullname" . }}
  labels:
    {{- include "test.labels" . | nindent 4 }}
spec:
  {{- toYaml .Values.plugin.values | nindent 2 }}
{{- end }}
```  
</details>

### Deleting the Release

```bash
$ helm delete demo
$ kubectl get pods
No resources found.
```

---

[Back to Index](#index)

---

# Chart Repositories

Adding and searching a third-party Helm Repository:

```bash
$ helm repo add bitnami https://charts.bitnami.com/bitnami
"bitnami" has been added to your repositories

$ helm search repo bitnami/
helm search repo bitnami/
NAME                                    CHART VERSION   APP VERSION     DESCRIPTION
bitnami/airflow                         6.7.1           1.10.12         Apache Airflow is a platform to programmaticall...
bitnami/apache                          7.6.0           2.4.46          Chart for Apache HTTP Server
bitnami/aspnet-core                     0.3.3           3.1.9           ASP.NET Core is an open-source framework create...

$ helm search repo wordpress
NAME                    CHART VERSION   APP VERSION     DESCRIPTION
bitnami/wordpress       9.9.2           5.5.3           Web publishing platform for building blogs and ...
```

Pulling a Helm-Chart

```bash
$ helm pull bitnami/wordpress
$ ls
wordpress-9.2.4.tgz

$ helm pull bitnami/wordpress --version 9.0.0
$ ls
wordpress-9.0.0.tgz wordpress-9.2.4.tgz

$ helm pull bitnami/wordpress --untar
$ ls
wordpress wordpress-9.0.0.tgz wordpress-9.2.4.tgz
```

---

[Back to Index](#index)

---

# Bonus: customizing a third-party Helm Chart

Combining what we just learned...

````bash
$ helm template wordpress bitnami/wordpress \
    --version 9.2.5 \
    --set service.type=NodePort \
    --set service.nodePortshttp=30001
```