# Test dependencies
In order to fulfill all the exercises in this training, you should have installed the following applications on your system:
- Azure CLI
  - https://docs.microsoft.com/en-us/cli/azure/install-azure-cli
- Git
  - https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- Kubectl
  - https://kubernetes.io/docs/tasks/tools/#kubectl
- Docker
  - https://www.docker.com/products/docker-desktop
- Helm
  - https://helm.sh/docs/intro/install/

To test the dependencies you can run the following commands and you should get a feedback:
```bash
az version
git version
kubectl version
docker version
```

# Sign in to Azure Account
We invited you to join our Azure tenant "Liquid Reply Training". You should have received an invitation mail to your Reply.de mailbox. Please make sure, that you have accepted the invitation. 

```bash
az login #You can now sign in through your web browser
az account set --subscription 0ae0b751-32d8-4e2e-af40-6532f9368a65 #To set the default subscription to the one we are using
az aks get-credentials --resource-group aks --name aks #Get the kubeconfig for our AKS cluster
```

# Check the connection to the Kubernetes cluster
Now you should be able to test the connection to the Kubernetes cluster. Running `kubectl version` will show a client version (your installed kubectl version) and a server version (the version of the Kubernetes cluster):
```bash
kubectl version

Client Version: version.Info{Major:"1", Minor:"21", GitVersion:"v1.21.3", GitCommit:"ca643a4d1f7bfe34773c74f79527be4afd95bf39", GitTreeState:"clean", BuildDate:"2021-07-15T21:04:39Z", GoVersion:"go1.16.6", Compiler:"gc", Platform:"darwin/amd64"}
Server Version: version.Info{Major:"1", Minor:"21", GitVersion:"v1.21.2", GitCommit:"802eff1fe87ad2dd737ebbe891f30500b88beb00", GitTreeState:"clean", BuildDate:"2021-11-15T08:35:41Z", GoVersion:"go1.16.5", Compiler:"gc", Platform:"linux/amd64"}
```


# Bonus
For later usage you might want to have a local Kubernetes cluster to test things out, play around, etc. 
If you have Docker already installed on your local machine, you just would need `kind` to achieve a local Kubernetes cluster.
- Install kinD: https://kind.sigs.k8s.io/docs/user/quick-start/

```bash
$ kind create cluster
Creating cluster "kind" ...
 ✓ Ensuring node image (kindest/node:v1.21.1) 🖼 
 ✓ Preparing nodes 📦  
 ✓ Writing configuration 📜 
 ✓ Starting control-plane 🕹️ 
 ✓ Installing CNI 🔌 
 ✓ Installing StorageClass 💾 
Set kubectl context to "kind-kind"
You can now use your cluster with:

kubectl cluster-info --context kind-kind

Thanks for using kind! 😊

$ kubectl get pods -A
NAMESPACE            NAME                                         READY   STATUS    RESTARTS   AGE
kube-system          coredns-558bd4d5db-sq7nc                     1/1     Running   0          36s
kube-system          coredns-558bd4d5db-vvfdd                     1/1     Running   0          36s
kube-system          etcd-kind-control-plane                      1/1     Running   0          51s
kube-system          kindnet-cw296                                1/1     Running   0          37s
kube-system          kube-apiserver-kind-control-plane            1/1     Running   0          44s
kube-system          kube-controller-manager-kind-control-plane   1/1     Running   0          44s
kube-system          kube-proxy-7zzp7                             1/1     Running   0          37s
kube-system          kube-scheduler-kind-control-plane            1/1     Running   0          51s
local-path-storage   local-path-provisioner-547f784dff-rz8pz      1/1     Running   0          36s

$ kubectl get nodes
NAME                 STATUS   ROLES                  AGE    VERSION
kind-control-plane   Ready    control-plane,master   115s   v1.21.1
``` 

It is also possible to create multiple Nodes. To do that, you have to provide a [cluster configuration file](https://kind.sigs.k8s.io/docs/user/quick-start/#advanced), e.g.:
```yaml
# a cluster with 3 control-plane nodes and 3 workers
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role: control-plane
- role: control-plane
- role: worker
- role: worker
- role: worker
```

```bash
$ kind create cluster --config cluster.yaml
Creating cluster "kind" ...
 ✓ Ensuring node image (kindest/node:v1.21.1) 🖼 
 ✓ Preparing nodes 📦 📦 📦 📦 📦 📦  
 ✓ Configuring the external load balancer ⚖️ 
 ✓ Writing configuration 📜 
 ✓ Starting control-plane 🕹️ 
 ✓ Installing CNI 🔌 
 ✓ Installing StorageClass 💾 
 ✓ Joining more control-plane nodes 🎮 
 ✓ Joining worker nodes 🚜 
Set kubectl context to "kind-kind"
You can now use your cluster with:

kubectl cluster-info --context kind-kind

Have a question, bug, or feature request? Let us know! https://kind.sigs.k8s.io/#community 🙂

$ kubectl get pods -A
NAMESPACE            NAME                                          READY   STATUS    RESTARTS   AGE
kube-system          coredns-558bd4d5db-bxpx8                      1/1     Running   0          2m41s
kube-system          coredns-558bd4d5db-zm9bf                      1/1     Running   0          2m41s
kube-system          etcd-kind-control-plane                       1/1     Running   0          2m41s
kube-system          etcd-kind-control-plane2                      1/1     Running   0          105s
kube-system          etcd-kind-control-plane3                      1/1     Running   0          48s
kube-system          kindnet-ccwhr                                 1/1     Running   0          54s
kube-system          kindnet-cmcnl                                 1/1     Running   0          2m12s
kube-system          kindnet-hcdhk                                 1/1     Running   0          69s
kube-system          kindnet-j7bjs                                 1/1     Running   0          54s
kube-system          kindnet-m9bqn                                 1/1     Running   0          54s
kube-system          kindnet-pd6f9                                 1/1     Running   0          2m30s
kube-system          kube-apiserver-kind-control-plane             1/1     Running   0          2m41s
kube-system          kube-apiserver-kind-control-plane2            1/1     Running   0          105s
kube-system          kube-apiserver-kind-control-plane3            1/1     Running   1          46s
kube-system          kube-controller-manager-kind-control-plane    1/1     Running   1          2m41s
kube-system          kube-controller-manager-kind-control-plane2   1/1     Running   0          105s
kube-system          kube-controller-manager-kind-control-plane3   1/1     Running   0          8s
kube-system          kube-proxy-4brhx                              1/1     Running   0          2m12s
kube-system          kube-proxy-98t4d                              1/1     Running   0          2m42s
kube-system          kube-proxy-9cdk5                              1/1     Running   0          54s
kube-system          kube-proxy-9rgm6                              1/1     Running   0          54s
kube-system          kube-proxy-gcm5m                              1/1     Running   0          54s
kube-system          kube-proxy-gjjfr                              1/1     Running   0          69s
kube-system          kube-scheduler-kind-control-plane             1/1     Running   1          2m41s
kube-system          kube-scheduler-kind-control-plane2            1/1     Running   0          105s
local-path-storage   local-path-provisioner-547f784dff-prpln       1/1     Running   0          2m27s

$ kubectl get nodes  
NAME                  STATUS   ROLES                  AGE     VERSION
kind-control-plane    Ready    control-plane,master   2m56s   v1.21.1
kind-control-plane2   Ready    control-plane,master   2m15s   v1.21.1
kind-control-plane3   Ready    control-plane,master   72s     v1.21.1
kind-worker           Ready    <none>                 57s     v1.21.1
kind-worker2          Ready    <none>                 57s     v1.21.1
kind-worker3          Ready    <none>                 57s     v1.21.1

```