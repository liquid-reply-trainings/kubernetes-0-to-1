# Kubernetes API Objects <!-- omit in toc -->

- [ConfigMaps](#configmaps)
  - [1. Create a ConfigMap](#1-create-a-configmap)
  - [2. Delete a ConfigMap](#2-delete-a-configmap)
- [Secrets](#secrets)
- [Deployment](#deployment)
  - [1. Deploy Initial App](#1-deploy-initial-app)
  - [2. Adjust version and Update the app](#2-adjust-version-and-update-the-app)
  - [3. Undo the deployment](#3-undo-the-deployment)
  - [4. Change the update strategy and conditions; and scale it up](#4-change-the-update-strategy-and-conditions-and-scale-it-up)


## ConfigMaps

### 1. Create a ConfigMap

You can create config maps from literals, files and so on. Yet we will create a configmap from a txt file. Adjust the message if you want!

```bash
kubectl create configmap welcome --from-file=hi=./*welcome.txt*
```

Check if the ConfigMap was correctly created

```bash
kubectl get cm
kubectl describe cm welcome
#describe will give you an output like this
Name:         welcome
Namespace:    default
Labels:       <none>
Annotations:  <none>

Data
====
hi:
----
hello hallo hi hej hola
Events:  <none>
```

Now you have a config map, so we can use it in a container.
Just apply the read_config.yaml `kubectl apply -f read_config.yaml`

Shortly investigate the Pod, what do you see?

If you are missing your Welcome Message run:

```bash
kubectl logs say-hey | grep WELCOME
```

### 2. Delete a ConfigMap

```bash
kubectl delete -f read_config.yaml
kubectl delete cm welcome
```

## Secrets

Secret works like config maps, you can use the secret.yaml or add your own secret and apply the yaml.

Inspect the secret like you did with the configmap:

```bash
Name:         topsecret
Namespace:    default
Labels:       <none>
Annotations:
Type:         Opaque

Data
====
password:  13 bytes
username:  8 bytes
```

Now copy read_config.yaml and call it read_secret.yaml. You have to do some small adjustments: our values don't come from a Config Map as Key Reference, but from a `secretKeyRef`. Also we have **two** secrets.

If you think you have a solution deploy your container and inspect the logs. Could you get your username and password??

```bash
kubectl logs say-hey | grep topsecret-pw
kubectl logs say-hey | grep topsecret-name
```

## Deployment

### 1. Deploy Initial App

Apply our test app `kubectl apply -f manifests/kuard.yaml`

### 2. Adjust version and Update the app

Update your kuard deployment by chaning the version to `purple` and apply kuard:
`kubectl apply -f manifests/kuard.yaml --record=true`

The `--record` flag gives you a command trail and therefore insights what happens to the deplyoment.

### 3. Undo the deployment

```bash
kubectl rollout undo deployment kuard-deployment
```

### 4. Change the update strategy and conditions; and scale it up

We will add now rolling upgrade staretgy to the deployment spec:

```yaml
strategy:
  rollingUpdate:
    maxSurge: 25%
    maxUnavailable: 25%
  type: RollingUpdate
```

```bash
kubectl scale --replicas=4 deployment kuard-deployment
```

Play around with the replicas (some ideas: 0, 10, ...)
