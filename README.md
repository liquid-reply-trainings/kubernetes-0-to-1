# Setup your Environment

## Preqrequisites

- Clone this Repository :)
- AWS Authenticator
    - AWS Credentials:
    - `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS IAM Role`
- kubectl


### How to clone?

```bash
$ git clone https://gitlab.com/liquid-reply-trainings/kubernetes-0-to-1.git liquid-reply-trainings/kubernetes-0-to-
$ cd liquid-reply-trainings/kubernetes-0-to-1
$ ls -lah
total 16K
drwxr-xr-x 15 cvoigt  480 Oct  6 13:42 ./
drwxr-xr-x  3 cvoigt   96 Oct  6 13:42 ../
drwxr-xr-x 12 cvoigt  384 Oct  6 13:42 .git/
-rw-r--r--  1 cvoigt  443 Oct  6 13:42 .gitignore
drwxr-xr-x  3 cvoigt   96 Oct  6 13:42 001_containers/
drwxr-xr-x  5 cvoigt  160 Oct  6 13:42 004_kubernetes_api_access/
drwxr-xr-x  4 cvoigt  128 Oct  6 13:42 005_k8s_api_objects/
drwxr-xr-x  4 cvoigt  128 Oct  6 13:42 006_ingress/
drwxr-xr-x  3 cvoigt   96 Oct  6 13:42 007_scheduling/
drwxr-xr-x  4 cvoigt  128 Oct  6 13:42 008_quality_of_service/
drwxr-xr-x  8 cvoigt  256 Oct  6 13:42 009_cluster_troubleshooting/
drwxr-xr-x  5 cvoigt  160 Oct  6 13:42 010_security/
drwxr-xr-x  3 cvoigt   96 Oct  6 13:42 011_packaging_applications/
drwxr-xr-x  3 cvoigt   96 Oct  6 13:42 013_best_practices/
-rw-r--r--  1 cvoigt 8.1K Oct  6 13:42 README.md
```

### Is my AWS Authenticator installed?

Check with

```bash
$ aws-iam-authenticator version
{"Version":"0.5.3","Commit":"a0516fb9ace571024263424f1770e6d861e65d09"}
```

If it is not installed, [please install it for your operating System](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html):

If you have a package manager installed you can do the following:

macOS:
```bash
$ brew install aws-iam-authenticator
```

Windows:
```bash
$ choco install -y aws-iam-authenticator
```

### Is kubectl installed?

Check with 

```bash
$ kubectl version
Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.1", GitCommit:"632ed300f2c34f6d6d15ca4cef3d3c7073412212", GitTreeState:"clean", BuildDate:"2021-08-19T15:38:26Z", GoVersion:"go1.16.6", Compiler:"gc", Platform:"darwin/amd64"}
error: You must be logged in to the server (the server has asked for the client to provide credentials)
```

If it is not installed, [please install it for your operating System](https://kubernetes.io/de/docs/tasks/tools/install-kubectl/).

macOS:
```bash
$ brew install kubernetes-cli
```

Windows:
```bash
$ choco install kubernetes-cli
```

---

## Configuring kubectl

Make sure you are in the root of our course repository!

```bash
$ pwd
/Users/cvoigt/git/gitlab.com/liquid-reply-trainings/kubernetes-0-to-1
```

### 1. Set the kubeconfig

The kubectl config we will use looks like this:

> Please note: we will have to adjust it accordingly!

```bash
cat <<EOF > kube_config
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM1ekNDQWMrZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJeE1UQXdNVEV3TVRNd01Gb1hEVE14TURreU9URXdNVE13TUZvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTW1hCm14Tmt3a3h5TldyaG8zV0pDbUs2dmpwUG9lRzcrekhhY3A3K0d0akxOZ00yalpGaytORUhHS3pSQ3lKTzZ2SnUKSlg5Z1hSbEt2SUZ6NkhWZm9ob0FUWlRWdW54bmNGcUpCaDZ1ejBDQkg4L1E5V0xYTGtJNmZQblpDcUJYVStVLwo3Q0pLYy9zcHdKZmRhaFNiczFKSGROalpRS2pxZW9UQzZOaTMxYWlWb1lMMFY1MVVXdWpZbHlPQmVKU044YVc3CjRKNlJZN2h2REUxWjF4Tyt0akxHL05wa1p6SHZsbFlzZENGalRzR0tRQldiTFJOVWVrL29qWVArNzVyMjhYSWsKZll1bUdJaTV5SHVtaWRWMysrN282WXJOcXhzd3dpS0pUczVmNVF3TTVTZDlwL3NndlB2NHY3Z091VGQ3VFVEegowYmZmS1RYNXdnTURNK2ZsVStVQ0F3RUFBYU5DTUVBd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0hRWURWUjBPQkJZRUZMdXdLSG03Q0RYYmpkdnlRSDhwTFVvSy9QbkRNQTBHQ1NxR1NJYjMKRFFFQkN3VUFBNElCQVFDYXErYzh6UjlNWFBJQW9hNThwZWR0N2NRbjd3R1Vzb2tUbW5sc21lbGQyOGxSZlBONwpLV2E2MytkNndYTWhkR2Q3ZlcyTUo2OU1yUDlTaVVFSjMxM0l2WFhFaG5wN3VxbDdJNlF2YnZDUmNOTlRwR20xCllEa1FybHBRNklSbm0zSEUyUG9tS2pWUGRjVFBYY00rekF2Y0R4bTh1T1hvZXg4OXpsREtzZ0ZmdUpsNWtza3IKSWZRNktwRUExSlRMV242SE5sbHpBS1J5SWZKYlp2NDNEZW1Sb2xWV3N1K2FkZUtURElyK3VqVnUzWFpaSVNFOQpIWlVmNU1YY1lMYkxxUzd3enFJM1FzNUd0OHNtaWVtMmlVWmRGNGV3TDhwYmRwSlpFOVJHa01qZ05iTHhBSmtvClNmQnQyelhPeEthMnRHZm4wUHlaUHhYMXRlMmNPLzNjN2ptZwotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    server: https://7403E5B502C13EC0DF481A9DAE183DC5.yl4.eu-west-1.eks.amazonaws.com
  name: eks_liquid-training-T61buzcS
contexts:
- context:
    cluster: eks_liquid-training-T61buzcS
    user: eks_liquid-training-T61buzcS
  name: eks_liquid-training-T61buzcS
current-context: eks_liquid-training-T61buzcS
kind: Config
preferences: {}
users:
- name: eks_liquid-training-T61buzcS
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      args:
      - token
      - -i
      - liquid-training-T61buzcS
      - -r
      - arn:aws:iam::793520626455:role/<your role>  # Change role here!
      command: aws-iam-authenticator
      env: null
      interactiveMode: IfAvailable
      provideClusterInfo: false
EOF
```

This should create a kube_config file in the root of our course directory:

```bash
$ ls -lah
total 16K
drwxr-xr-x 16 cvoigt  512 Oct  6 13:11 ./
drwxr-xr-x  3 cvoigt   96 Oct  6 12:01 ../
drwxr-xr-x 12 cvoigt  384 Oct  6 13:13 .git/
-rw-r--r--  1 cvoigt  431 Oct  6 12:01 .gitignore
drwxr-xr-x  3 cvoigt   96 Oct  6 12:06 001_containers/
drwxr-xr-x  5 cvoigt  160 Oct  6 12:06 004_kubernetes_api_access/
drwxr-xr-x  4 cvoigt  128 Oct  6 12:06 005_k8s_api_objects/
drwxr-xr-x  4 cvoigt  128 Oct  6 12:06 006_ingress/
drwxr-xr-x  3 cvoigt   96 Oct  6 12:01 007_scheduling/
drwxr-xr-x  4 cvoigt  128 Oct  6 12:01 008_quality_of_service/
drwxr-xr-x  8 cvoigt  256 Oct  6 12:01 009_cluster_troubleshooting/
drwxr-xr-x  5 cvoigt  160 Oct  6 12:01 010_security/
drwxr-xr-x  3 cvoigt   96 Oct  6 12:01 011_packaging_applications/
drwxr-xr-x  3 cvoigt   96 Oct  6 12:01 013_best_practices/
-rw-r--r--  1 cvoigt 2.2K Oct  6 13:10 kube_config      <-- this is our config!
-rw-r--r--  1 cvoigt 4.2K Oct  6 13:11 setup.md
```

> If you haven't done yet, please adjust your IAM Role accordingly!

Now, let kubectl know, where to find its config!

```bash
export KUBECONFIG=$(pwd)/kube_config
```

Alternatively you can make the path explicit (following the example from above):

```bash
export KUBECONFIG="/Users/cvoigt/git/gitlab.com/liquid-reply-trainings/kubernetes-0-to-1/kube_config"
```

And check if you set the environment variable properly:

```bash
$ ls -lah $KUBECONFIG
-rw-r--r-- 1 cvoigt 2.2K Oct  6 13:10 /Users/cvoigt/git/gitlab.com/liquid-reply-trainings/kubernetes-0-to-1/kube_config
```

This should output your kubeconfig:

```bash
$ kubectl config view
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://7403E5B502C13EC0DF481A9DAE183DC5.yl4.eu-west-1.eks.amazonaws.com
  name: eks_liquid-training-T61buzcS
contexts:
- context:
    cluster: eks_liquid-training-T61buzcS
    user: eks_liquid-training-T61buzcS
  name: eks_liquid-training-T61buzcS
current-context: eks_liquid-training-T61buzcS
kind: Config
preferences: {}
users:
- name: eks_liquid-training-T61buzcS
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      args:
      - token
      - -i
      - liquid-training-T61buzcS
      - -r
      - arn:aws:iam::793520626455:role/<your role>
      command: aws-iam-authenticator
      env: null
      interactiveMode: IfAvailable
      provideClusterInfo: false
```

Open the `kube_config` file and adjust your role accordingly!


And this should output the version and a notice about a missing token.

```bash
$ kubectl version
could not get token: NoCredentialProviders: no valid providers in chain. Deprecated.
        For verbose messaging see aws.Config.CredentialsChainVerboseErrors
Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.1", GitCommit:"632ed300f2c34f6d6d15ca4cef3d3c7073412212", GitTreeState:"clean", BuildDate:"2021-08-19T15:38:26Z", GoVersion:"go1.16.6", Compiler:"gc", Platform:"darwin/amd64"}
Unable to connect to the server: getting credentials: exec: executable aws-iam-authenticator failed with exit code 1
```

This is because kubectl asks the AWS IAM Authenticator for an access token. But the Authenticator does not have any credentials to authorize us... Lets fix this!

### 2. Set your AWS Credentials

The AWS IAM Authenticator assumes that an AWS Access Key and a Secret Access key are provided either via the `~/.aws/credentials` file or directly exposed via environment variables. For now, we will use environment variables:

```bash
export AWS_ACCESS_KEY_ID="<your access key>"
export AWS_SECRET_ACCESS_KEY="<your secret access key>"
```

Check if you exported the environment variables correctly

```bash
$ echo $AWS_ACCESS_KEY_ID
<your access key>

$ echo $AWS_SECRET_ACCESS_KEY
<your secret access key>
```

### 3. Check if everything is setup!

If you did everything correctly, the output of the following commands should look similar:

```bash
$ kubectl version
Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.1", GitCommit:"632ed300f2c34f6d6d15ca4cef3d3c7073412212", GitTreeState:"clean", BuildDate:"2021-08-19T15:38:26Z", GoVersion:"go1.16.6", Compiler:"gc", Platform:"darwin/amd64"}
Server Version: version.Info{Major:"1", Minor:"21+", GitVersion:"v1.21.2-eks-0389ca3", GitCommit:"8a4e27b9d88142bbdd21b997b532eb6d493df6d2", GitTreeState:"clean", BuildDate:"2021-07-31T01:34:46Z", GoVersion:"go1.16.5", Compiler:"gc", Platform:"linux/amd64"}
```

```bash
$ kubectl cluster-info
Kubernetes control plane is running at https://7403E5B502C13EC0DF481A9DAE183DC5.yl4.eu-west-1.eks.amazonaws.com
CoreDNS is running at https://7403E5B502C13EC0DF481A9DAE183DC5.yl4.eu-west-1.eks.amazonaws.com/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```