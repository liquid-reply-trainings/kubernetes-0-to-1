# Networking

## Container-to-container communication

Create Pod "netshoot", which consists of a container `server` and a container `netshoot`.

```bash
$ kubectl -n <your-namespace> apply -f manifests/networking/container2container.yaml
pod/netshoot created

$ kubectl -n <your-namespace> get po -owide
NAME       READY   STATUS    RESTARTS   AGE   IP           NODE                          NOMINATED NODE   READINESS GATES
netshoot   2/2     Running   0          7s    10.0.0.130   aks-aks-14835901-vmss00000b   <none>           <none>
```

`server` will start a simple HTTP server and serve an index.html file.

We will use `netshoot` to curl this html file via localhost:

```bash
$ kubectl -n <your-namespace> exec -it netshoot -c netshoot -- curl http://0.0.0.0:8000
<h1>Hello from 10.0.0.130</h1>
```

If everything works correct, you indeed get the HTML file.

Check if you can use the pods IP Address to curl the index.html file:

```bash
$ kubectl -n <your-namespace> exec -it netshoot -c netshoot -- curl http://10.0.0.130:8000
<h1>Hello from 10.0.0.130</h1>
```

You can visit the "website" via port-forwarding the pod:

```bash
$ kubectl -n <your-namespace> port-forward pod/netshoot 8000
Forwarding from 127.0.0.1:8000 -> 8000
Forwarding from [::1]:8000 -> 8000
```

Now, open your browser at `localhost:8000` or `127.0.0.1:8000`.

Bonus question: how does the Pod get its own IP address?
Answer: https://kubernetes.io/docs/tasks/inject-data-application/environment-variable-expose-pod-information/#the-downward-api

## Pod-to-pod communication

Lets how we can address `pod2pod-b` from within `pod2pod-a`. Again `server` will start a simple HTTP server and serve an index.html on `pod2pod-b`, while we will use `netshoot` to curl this html file.

```bash
$ kubectl -n <your-namespace> apply -f manifests/networking/pod2pod.yaml
pod/pod2pod-a created
pod/pod2pod-b created

$ kubectl -n <your-namespace> get po -owide
NAME        READY   STATUS        RESTARTS   AGE     IP           NODE                          NOMINATED NODE   READINESS GATES
pod2pod-a   2/2     Running       0          5m14s   10.0.0.156   aks-aks-14835901-vmss00000b   <none>           <none>
pod2pod-b   2/2     Running       0          5m13s   10.0.1.8     aks-aks-14835901-vmss000009   <none>           <none>
```

Lets adjust the command from the previous example to exec into `pod2pod-a` and curl `pod2pod-b`. Make sure to get the correct IP from the `get po -owide` output.

```bash
$ kubectl -n <your-namespace> exec -it pod2pod-a -c netshoot -- curl http://<insert-ip>:8000
<h1>Hello from 10.0.1.8 on pod2pod-b</h1>
```

Every pod also gets a DNS entry, which makes it possible to access the pod by calling its DNS name. Be careful to subsitute all `.` of the IP address by `-`.

```bash
$ kubectl -n <your-namespace> exec -it pod2pod-a -c netshoot -- curl http://10-0-1-8.<your-namespace>.pod.cluster.local:8000
<h1>Hello from 10.0.1.8 on pod2pod-b</h1>
```

Yes... you still need to know the IP address of the pod...

Bonus: Its not used very often in practice, but if you really need to query a single pod via its A/AAAA record, make sure to read some more details: https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pods

## Pod-to-Service communication

Now we will see how to address multiple pods at once using a service resource.

Therefore we have a pod called `pod2service-listener` which contains `netshoot` that we will use to curl the other pods called `pod2service-server`.

This time, `pod2service-server` is managed by a deployment and has a replica of 3. A service will load balance our requests to the pods.

```bash
$ kubectl -n <your-namespace> apply -f manifests/networking/pod2service.yaml
pod/pod2service-listener created
deployment.apps/pod2service-server created
service/pod2service-service created

kubectl -n <your-namespace> get po -owide
NAME                                  READY   STATUS    RESTARTS   AGE   IP           NODE                          NOMINATED NODE   READINESS GATES
pod2service-listener                  1/1     Running   0          15s   10.0.0.106   aks-aks-14835901-vmss00000b   <none>           <none>
pod2service-server-754b894857-f6jw7   1/1     Running   0          14s   10.0.0.109   aks-aks-14835901-vmss00000b   <none>           <none>
pod2service-server-754b894857-q96qq   1/1     Running   0          14s   10.0.0.232   aks-aks-14835901-vmss000009   <none>           <none>
pod2service-server-754b894857-tgk88   1/1     Running   0          14s   10.0.0.154   aks-aks-14835901-vmss00000b   <none>           <none>

kubectl -n <your-namespace> get svc
```

To see our service in action, we can port-forward it:

```bash
$ kubectl port-forward -n <your-namespace> service/pod2service-service 8000
```

Check your browser on `localhost:8000` or `127.0.0.1:8000`.

But how to address this service from within the cluster? Like this:

```bash
$ kubectl -n <your-namespace> exec -it pod2service-listener -c netshoot -- curl http://pod2service-service:8000
(... long output ...)
```

Alternative DNS names of your service:

- `pod2service-service`
- `pod2service-service.<your-namespace>`
- `pod2service-service.<your-namespace>.svc`
- `pod2service-service.<your-namespace>.svc.cluster.local`

Go and check your service in more detail - give special attention to the endpoints. Do you recognize the IPs? 

```bash
$ kubectl -n <your-namespace> describe service pod2service-service
Name:              pod2service-service
Namespace:         <your-namespace>
Labels:            app=pod2service
Annotations:       <none>
Selector:          app=pod2service,role=server
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                10.0.2.128
IPs:               10.0.2.128
Port:              8080  8000/TCP
TargetPort:        8080/TCP
Endpoints:         10.0.0.109:8080,10.0.0.154:8080,10.0.0.232:8080
Session Affinity:  None
Events:            <none>
```

We can see the endpoints in closer detail:

```
$ kubectl -n <your-namespace> describe ep pod2service-service
```

Lets check how fast those endpoints can change! Make sure to still watch your describe (`watch -n1 "kubectl ..."`).

```bash
$ kubectl -n <your-namespace> scale --replicas=1 deployment pod2service-server
```

Congratulations, you just learned how to cluster-internally address workloads!

---

# Ingress

How can we expose an application that is running in our Kubernetes cluster?

Note: Please create a new Pod by:

```bash
$ kubectl -n <your-namespace> run hello-world --restart=Never --image=liquidreply.azurecr.io/redhattraining/hello-world-nginx:v1.0
```

## ClusterIP Service with port-forwarding

Apply the YAML manifest `manifests/service-clusterip.yaml` to create a service
with type `ClusterIP`. This service is only reachable from within the
Kubernetes cluster using either the Service IP or its DNS name
(<svc-name>.<namespace>.svc.cluster.local).

The file has the following content, it should look familiar from the chapter
about services:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: hello-world-clusterip
spec:
  selector:
    run: hello-world
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
```

To control, if the Service has an active endpoint (Pod), we can check the output of `kubectl get endpoint`:
```bash
$ kubectl get ep
NAME                    ENDPOINTS                                                     AGE
hello-world-clusterip   10.0.0.68:8080                                                12s
kuard-service           10.0.0.124:8080,10.0.0.149:8080,10.0.0.243:8080 + 1 more...   138m
nginx                   <none>                                                        16m
```

In order to access our service now from outside the cluster, we can run the following command:

```bash
$ kubectl port-forward -n <namespace> svc/<service-name> <local-port>:<service-port>
```

or in this case:

```bash
$ kubectl port-forward svc/hello-world-clusterip 8080:80
```

This command will forward  port 8080 on your local machine to port 80 on the
service IP. The forwarding itself will go through the kube-apiserver.

You can now open your web browser on your local machine and point it to
`http://localhost:8080` to see the "Hello World" page.

While port-forwarding might work in a debugging or development scenario, the
kube-apiserver is not made to be able to handle production traffic.

Now let´s create a second container `client-busybox` and check, if it is possible to interact with the hello-world Pod:
```bash
$ kubectl run client-busybox --rm --image=liquidreply.azurecr.io/busybox:latest -it --restart=Never -- sh
wget -O- hello-world-clusterip:80
```
If you want to access the Service from another namespace, you will have to use `service-name.namespace-name:port`.

## LoadBalancer service

Services with type LoadBalancer, by themselves do not do anything. Instead,
they require the Kubernetes cluster to be properly configured for use with a
cloud provider. In our case, when we set up the Kubernetes cluster, we already
configured all necessary parts to integrate with Azure in order for us to create
a Load Balancer from within Kubernetes.

To expose our hello-world pod through a LoadBalancer, apply the manifest
`manifests/service-loadbalancer.yaml`, which contains the following service:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: hello-world-loadbalancer
spec:
  type: LoadBalancer
  selector:
    run: hello-world
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
```

When you describe the service afterwards in the cluster, you might notice a
small addition that we did not configure: a nodePort was set for our service
port 80!

This is because when you create a service with type LoadBalancer, we also
automatically open a random nodePort for the service. The LoadBalancer then
directs its traffic to that nodePort.

In addition, the azure cloud-provider also created the necessary firewall
rules to allow traffic from the internet to reach the LoadBalancer and from
there to the nodes.

With that IP, we can now also reach our hello-world pod, by opening a browser and
pointing it to the LoadBalancer URL.

## Ingress controller with LoadBalancer

Ingress controllers act as
reverse proxies in your cluster. An often used example for a Kubernetes Ingress
controller is the NGINX Ingress Controller (there are actually two NGINX
ingress controllers: [one by the Kubernetes
community](https://github.com/kubernetes/ingress-nginx), and [one by NGINX
themselves](https://github.com/nginxinc/kubernetes-ingress); most often when
people talk aber the NGINX ingress controller, they mean the one by the
Kubernetes community).

Configuration of the Ingress controllers is done through the `Ingress`
Kubernetes resource.

First let's create a Kubernetes ingress resource for our `hello-world-clusterip`
service. You can find the YAML manifest in `manifests/hello-world-ingress.yaml`:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: hello-world-ingress
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - path: /<your-name>
        pathType: Prefix
        backend:
          service:
            name: hello-world-clusterip
            port:
              number: 80
```

This configuration will configure an Ingress controller to route the path
prefix `/` to the service `hello-world-clusterip` on port `80`. 

In the same way, you can add additional paths that  point to different services.

```bash
$ kubectl -n <your-namespace> describe ingress hello-world-ingress
Name:             hello-world-ingress
Namespace:        namespace-f-stoeber12
Address:          20.79.246.86
Default backend:  default-http-backend:80 (<error: endpoints "default-http-backend" not found>)
Rules:
  Host        Path  Backends
  ----        ----  --------
  *
              /christoph   hello-world-clusterip:80 (10.0.0.179:8080)
Annotations:  kubernetes.io/ingress.class: nginx
              nginx.ingress.kubernetes.io/rewrite-target: /
Events:
  Type    Reason  Age                    From                      Message
  ----    ------  ----                   ----                      -------
  Normal  Sync    7m47s (x3 over 9m55s)  nginx-ingress-controller  Scheduled for sync
```

The address is a different LoadBalancer than the one we created before. This
LoadBalancer was created by another service that was created for the NGINX
ingress controller. We can now use the LoadBalancer address to access our
pod.

To get the LoadBalancer IP, check the services in the `ingress` namespace:

```bash
kubectl -n ingress get svc
NAME                                            TYPE           CLUSTER-IP   EXTERNAL-IP    PORT(S)                      AGE
ingress-nginx-controller-controller             LoadBalancer   10.0.2.149   20.79.246.86   80:31431/TCP,443:31589/TCP   12d
ingress-nginx-controller-controller-admission   ClusterIP      10.0.2.109   <none>         443/TCP                      12d
```

So your service should be available at `http://20.79.246.86/<your name>`.

Compared to before, we now have the advantage that we only need one
LoadBalancer for multiple services, where we can expose them through different
domain names or through different URL paths.

NOTE: If you want to use your own domains for your exposed services, you will
need to point them to the LoadBalancer DNS record. This is not done
automatically by Kubernetes, but it is possible to automate this by using e.g.
[external-dns](https://github.com/kubernetes-sigs/external-dns).

## Links

- https://kubernetes.io/docs/concepts/services-networking/service/
- https://kubernetes.io/docs/concepts/services-networking/ingress/
- https://github.com/kubernetes/ingress-nginx
- https://github.com/kubernetes-sigs/external-dns
