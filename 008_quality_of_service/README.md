# Quality of Service

## Resource requests and limits

Resource requests are used by Kubernetes to determine if a pod can be scheduled
on a specific node. It is also the amount of CPU/memory that is reserved for
the application and that the application can safely use without the risk of
resource exhaustion.

Resource limits work differently for CPU and memory:

- For memory, if the application uses more memory than specified in the limit,
  the pod will be OOMKilled by Kubernetes, in order to prevent the application
  from taking up all the memory of the node.
- For CPU, a pod will not be killed when reaching it's CPU limit. Instead, the
  CPU time will simply be capped for the application, meaning a pod with a 100m
  CPU limit will, at most, run for 0.1s per s. Essentially it will slow the
  application down.

Defining resource requests and limits is crucial to avoid cascading node
failures, where a pod crashes a node by taking up all its memory, then being
scheduled to the next working node and repeat the same process until all nodes
are no longer working.

To test resource limits, apply the manifest `kuard-resources.yaml` from the manifests directory: `kubectl apply -f manifests/kuard-resources.yaml`

Then forward the port 8080 to your local machine and open `http://localhost:8080` in your browser: `kubectl port-forward deployment/kuard 8080:8080`

In the main menu choose the Memory page. The manifest you applied contained a
memory limit of 300MB, so clicking the "Allocate 500MB" link will cause
Kubernetes to OOM kill the pod. If you want to observe this behavior, in
another terminal, run `kubectl get pod -w`.

Cleanup:
```bash
kubectl delete deploy kuard
```

<!-- ## Pod Disruption Budget

Pod Disruption Budgets allow to specify how many pods of a
deployment/daemonset/statefulset may be disrupted (i.e. non-functional).

If, for example, a deployment with 3 pods is configured with a disruption
budget that sets the minimum available pods to 2, and one of the pods is "down"
(e.g. crashing or in a pending state), no other pod of the deployment may be
removed, as it would violate the policy set in the PodDisruptionBudget.

These removals of pods normally happen during a rolling update of a deployment,
or when pods are evicted from nodes. In these cases, the rolling update or
eviction would be paused for the pods of this deployment, until the third pod
becomes available, in which case, by removing a pod, the policy is still
fulfilled.

PodDisruptionBudgets do not protect against hardware failures or application
crashes - only against the removal of pods that is controlled by Kubernetes.

Create a new kuard Deplyoment and PDB:
```bash
cat manifests/kuard-pdb.yaml
kubectl apply -f manifests/kuard-pdb.yaml
```

Cleanup:
```bash
kubectl delete deploy kuard
kubectl delete pdb kuard
``` -->
## Liveness, Readiness & Startup probes

Liveness and readiness probes are mechanisms to detect when a pod is not
working as expected. A liveness probe is used to detect if a pod (or rather the
application within) is running as expected. A readiness probe indicates whether
or not a pod is ready to receive network traffic. A startup probe indicates to
Kubernetes that the application has started and can now properly respond to
liveness probes. This comes in handy when an application might take a very long
time to initially start. In that case, if liveness probes were started
immediately, the pod might be wrongly marked as dead.

All checks can be defined in similar ways:

- with a command that is executed inside the container
- with an HTTP call
- with a TCP connection attempt

### Examples for some probes

A liveness probe using a HTTP GET request to the /healthz endpoint:
```yaml
livenessProbe:
  httpGet:
    path: /healthz
    port: liveness-port
  failureThreshold: 1
  periodSeconds: 10
```

A readiness probe using a command that is executed in the container:
```yaml
readinessProbe:
  exec:
    command:
    - cat
    - /tmp/healthy
  initialDelaySeconds: 5
  periodSeconds: 5
```

A startup probe using a TCP connection attempt:

```yaml
startupProbe:
  tcpSocket:
    port: 8080
  failureThreshold: 30
  periodSeconds: 10
```
Deploy the Probe-Deployment
```bash
cat manifests/kuard-probes.yaml
kubectl apply -f manifests/kuard-probes.yaml
kubectl port-forward deployment/kuard 8080:8080
```

Use the web UI to trigger liveness and readiness probe failures and observe what is happening with the Pod
<!-- ## Priority classes 

Priority classes are used to determine which pod to evict when not enough
resources are available to schedule a pod. Each pod has a specific priority. If
A pod with a high priority should be scheduled but there are not enough
resources available, Kubernetes will evict a pod with a lower priority to make
space for the high priority pod.

It is also possible to create priority classes with a `PreemptionPolicy:
Never`, that will not cause what was described above, but instead will only
place higher priority pods before lower priority pods in the scheduling queue.

Setting priority classes appropriately for pods can help to prevent important
pods (e.g. cluster-critical or customer facing) from getting evicted in favor
of less important pods (e.g. certificat renewal, which usually has a few days
lead time).


 -->
## Links

- https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/
- https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/
- https://kubernetes.io/docs/concepts/workloads/pods/disruptions/#pod-disruption-budgets
