1. Clone this repo
2. Build all binaries: make all-fakever-build
3. Build all docker images: docker build -f Dockerfile.color -t name:color .
4. Run webserver and test
5. Tag the docker image: acr:/name:blue acr:/name:green acr:/name:purple -> docker tag kuard:purple fstoeber.azurecr.io/kuard:purple
6. Push to acr -> docker push fstoeber12.azurecr.io/kuard:purple 




# Reference
This directory is a copy of https://github.com/kubernetes-up-and-running/kuard, which is licensed under Apache 2.0 license. 