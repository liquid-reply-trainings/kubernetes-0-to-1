# Containers <!-- omit in toc -->

- [Docker Intro](#docker-intro)
  - [Using Docker](#using-docker)
    - [A non-interactive container](#a-non-interactive-container)
    - [Running a container in the background](#running-a-container-in-the-background)
    - [Viewing container logs](#viewing-container-logs)
    - [Stopping a container](#stopping-a-container)
  - [Building Container Images](#building-container-images)
  - [Tag and Push Container Images](#tag-and-push-container-images)

---

## Docker Intro

### Using Docker

Lets check what we can do with docker:

```bash
$ docker info      # Display system-wide information
$ docker images    # List images
$ docker ps        # List containers
```

Lets pull our first image:

```bash
$ docker pull jpetazzo/clock
Using default tag: latest
latest: Pulling from jpetazzo/clock
0f8c40e1270f: Pull complete
Digest: sha256:ace75dda37174abb563799a8b9b2043505619559fe1120a26a63363dc48bcd26
Status: Downloaded newer image for jpetazzo/clock:latest
docker.io/jpetazzo/clock:latest

$ docker images
REPOSITORY                           TAG                                              IMAGE ID            CREATED             SIZE
jpetazzo/clock                       latest                                           59e5463e3307        12 months ago       1.22MB

$ docker rmi jpetazzo/clock    # delete image (if you try it, please pull again :) )
```

#### A non-interactive container

This container displays the time every second.

```bash
$ docker run jpetazzo/clock
Sun Jan 12 14:30:52 UTC 2020
Sun Jan 12 14:30:53 UTC 2020
Sun Jan 12 14:30:54 UTC 2020
```

- it will run forever until it is killed
- `^C`to stop it

#### Running a container in the background

```bash
$ docker run -d jpetazzo/clock
f31615d81aa4a91ee9cf90a91d0654d8bcb13bfbac7a11ecf0ccb4d42e53e9c6
```

To see that the container is running:

```bash
$ docker ps
```

- (truncated) ID of the containers
- image used to start the container
- uptime and other information as ports being used, etc.
- Use `-l` flag to see the last started container
- Use `-a` to see also stopped containers
- Use `-n3`to see only last 3

#### Viewing container logs

```bash
$ docker logs <container-id>
```

- use `--tail 3` to get the last 3 events
- use `-f` to follow the log

#### Stopping a container

```bash
$ docker kill <container-id>
```

- stops the container immediately, by using the `KILL` signal
- a `KILL` signal cannot be intercepted and will forcibly terminate the container

```bash
$ docker stop <container-id>
```

- sends a `TERM` signal to stop a container gracefully
- if the container has not been stopped after 10 seconds, it will get killed

### Building Container Images
If we want to put our own applications in a docker container, we have to write our own Dockerfile and build the image based on the Dockerfile. 
As we do not have time to write our own application and Dockerfile in this training, this is already prepared. In the `kuard` directory we find an example application based on goLang. We will build three binaries and three docker images based on this application.

At first we build all three binaries:
```bash
$ cd kuard                  # Change directory to kuard
$ make all-fakever-build    # This command will compile the project to three binaries (blue, green, purple)
```

Now we are able to build all three docker images.
```bash
$ cat Dockerfile.blue                                             # Display the Dockerfile for the blue container image. Please review and undestand it.
$ docker build -f Dockerfile.blue -t kuard:blue .                  # This command will build the container image
$ docker build -f Dockerfile.green -t kuard:green .                # This command will build the container image
$ docker build -f Dockerfile.purple -t kuard:purple .              # This command will build the container image
$ docker run --rm -ti --name kuard --publish 8080:8080 kuard:blue # Run the container and have a look on localhost:8080
```
### Tag and Push Container Images
It is fine to build and run a container image on your local machine, but maybe you want to share it? Or you want to run it on your cloud environment? 
Then you should push it to a container registry like DockerHub, quay.io or (most likely) in your private container registry (ACR, ECR, GCR). In this exercise we will push the image to your container registry in our Azure tenant. Please have a look at the [Azure Portal](https://portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.ContainerRegistry%2Fregistries) and find your container registry.


Now we just have to tag and push the image. 
```bash
$ az acr login -n <yourname>                              # Login with your Docker CLI to your ACR
$ docker tag kuard:blue <yourname>.azurecr.io/kuard:blue  # Tag the local kuard:blue image with your registry name
$ docker push <yourname>.azurecr.io/kuard:blue            # Push the image
$ docker tag kuard:green <yourname>.azurecr.io/kuard:green
$ docker push <yourname>.azurecr.io/kuard:green
$ docker tag kuard:purple <yourname>.azurecr.io/kuard:purple
$ docker push <yourname>.azurecr.io/kuard:purple
```

Now you are able to see the images in your Azure Portal.

---

[Back to Index](#index)

---
