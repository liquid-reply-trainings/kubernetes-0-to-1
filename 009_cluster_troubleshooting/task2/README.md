### Solution

Check the Pod status:

```bash
$ kubectl get po
NAME         READY   STATUS     RESTARTS   AGE
broken-pod   0/1     Init:0/1   0          12s
```

How are the containers doing?

```bash

Init Containers:
  busybox-init-container:
    Container ID:  docker://f5356ac021fd2c95fca327baad3f303e27db24d4406712c441aabf8409631c62
    Image:         busybox
    Image ID:      docker-pullable://busybox@sha256:a9286defaba7b3a519d585ba0e37d0b2cbee74ebfe590960b0b1d6a5e97d1e1d
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
      -c
      until nslookup pods-init-container-service-nonexistent.$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace).svc.cluster.local; do echo waiting for myservice; sleep 2
; done
    State:          Running                     <-- container is running
      Started:      Thu, 12 Nov 2020 22:55:49 +0100
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-nqqgq (ro)
Containers:
  busybox-container:
    Container ID:
    Image:         busybox
    Image ID:
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
      -c
      echo The app is running! && sleep 3600
    State:          Waiting                   <-- container is waiting
      Reason:       PodInitializing
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-nqqgq (ro)
[...]
```

Lets check the logs of the init-container:

```bash
kubectl logs broken-pod -c busybox-init-container
Server:         10.96.0.10
Address:        10.96.0.10:53

** server can't find pods-init-container-service-nonexistent.default.svc.cluster.local: NXDOMAIN

*** Can't find pods-init-container-service-nonexistent.default.svc.cluster.local: No answer

waiting for myservice
Server:         10.96.0.10
Address:        10.96.0.10:53
```

The pod does not start, because the init-container prevents the main container from starting.

The init-container only executes successfully once it is able to resolve `pods-init-container-service-nonexistent.default.svc.cluster.local`.

So a valid solution is to create this service:

```bash
$ kubectl create svc clusterip pods-init-container-service-nonexistent --tcp=8080
```
