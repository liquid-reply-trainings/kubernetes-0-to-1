### Solution

If you `get po` you will see either of three states, depending at what time you check the status:

```bash
$ kubectl get po
NAME         READY   STATUS              RESTARTS   AGE
memory-pod   0/1     ContainerCreating   0          3s

$ kubectl get po
NAME         READY   STATUS      RESTARTS   AGE
memory-pod   0/1     OOMKilled   0          6s

$ kubectl get po
NAME         READY   STATUS             RESTARTS   AGE
memory-pod   0/1     CrashLoopBackOff   1          11s
```

You can check a containerst last state to check its Exit Code + Reason:

```bash
$ kubectl describe po | grep -A4 "Last State"
    Last State:     Terminated
      Reason:       OOMKilled
      Exit Code:    1
      Started:      Thu, 12 Nov 2020 23:08:20 +0100
      Finished:     Thu, 12 Nov 2020 23:08:20 +0100
```

You can fix the pod by adding a bit more memory :)