# Cluster Troubleshooting

> **IMPORTANT:** Please do not check the contents of the `task` subdirectories yet, as they will reveal solutions to our problems :)

## Index
* [Tooling](#tooling)
  * [Cluster Observability with Prometheus and Grafana](#cluster-observability-with-prometheus-and-grafana)
  * [k9s](##k9s)
  * [Octant](##octant)
* [Task-1](#task-1)
* [Task-2](#task-2)
* [Task-3](#task-3)
* [How to recover from a broken cluster](#how-to-recover-from-a-broken-cluster)
  * [A Kubernetes worker node has failed](#a-kubernetes-worker-node-has-failed)
  * [A Kubernetes master node has failed](#a-kubernetes-master-node-has-failed)
  * [etcd has failed](#etcd-has-failed)
  * [I'm not quite sure what broke](#Im-not-quite-sure-what-broke)

---
# Tooling

While `kubectl` is an important to for cluster interaction, it often is not enough to keep an overview. In this chapter we will present you three tools we are regularly using in production.

---

## Cluster Observability with Prometheus and Grafana

Prometheus and Grafana are the dream team for Kubernetes monitoring.

There are probably a hundred ways to deploy Prometheus/Grafana. Therefore we prepared an (opinionated) setup based on the kube-prometheus project.

* Github: https://github.com/prometheus-operator/kube-prometheus

![](./images/grafana.png)

### Installation

To install the complete setup we prepared for you, please use the following commands:

```bash
$ kubectl create -f monitoring_stack/kubernetes0to1/setup
$ kubectl create -f monitoring_stack/kubernetes0to1
$ kubectl create -f monitoring_stack/grafana/
$ kubectl create -f monitoring_stack/metricsserver/
```

This simple monitoring stack is based on [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus) and provides a Prometheus to collect data and Grafana to visualize data (... and some other components that are necessary to do so :) ).

Notice, that this created a new `namespace` "monitoring":

```bash
$ kubectl get ns
NAME              STATUS   AGE
default           Active   137m
kube-node-lease   Active   137m
kube-public       Active   137m
kube-system       Active   137m
monitoring        Active   129m
```

And installed quite a few resources:

```bash
$ kubectl -n monitoring get ds,deploy,svc,servicemonitor
NAME                           DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
daemonset.apps/node-exporter   3         3         3       3            3           kubernetes.io/os=linux   131m

NAME                                  READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/grafana               1/1     1            1           130m
deployment.apps/kube-state-metrics    1/1     1            1           131m
deployment.apps/prometheus-operator   1/1     1            1           131m

NAME                          TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)             AGE
service/grafana               ClusterIP   10.98.186.5     <none>        3000/TCP            130m
service/kube-state-metrics    ClusterIP   None            <none>        8443/TCP,9443/TCP   131m
service/node-exporter         ClusterIP   None            <none>        9100/TCP            131m
service/prometheus-k8s        ClusterIP   10.110.134.62   <none>        9090/TCP            131m
service/prometheus-operated   ClusterIP   None            <none>        9090/TCP            130m
service/prometheus-operator   ClusterIP   None            <none>        8443/TCP            131m

NAME                                                           AGE
servicemonitor.monitoring.coreos.com/coredns                   131m
servicemonitor.monitoring.coreos.com/grafana                   130m
servicemonitor.monitoring.coreos.com/kube-apiserver            131m
servicemonitor.monitoring.coreos.com/kube-controller-manager   131m
servicemonitor.monitoring.coreos.com/kube-scheduler            131m
servicemonitor.monitoring.coreos.com/kube-state-metrics        131m
servicemonitor.monitoring.coreos.com/kubelet                   131m
servicemonitor.monitoring.coreos.com/node-exporter             131m
servicemonitor.monitoring.coreos.com/prometheus                131m
servicemonitor.monitoring.coreos.com/prometheus-operator       131m
```

To access Grafana forward the service port to your local machine:

```bash
$ kubectl -n monitoring port-forward svc/grafana 3000
```

And access it in the browser via `http://localhost:3000`.

Prometheus can be access similarly via

```bash
$ kubectl -n monitoring port-forward svc/prometheus-k8s 9090
```

and accessed in the browser via `http://localhost:9090`.

---

## k9s

A terminal-based graphical user interface for Kubernetes. It is meant to be used as a `kubectl` substitute. Due to the fact that it is CLI-based, in can be installed on servers as well :)

* Website: https://k9scli.io/
* Github: https://github.com/derailed/k9s

![](./images/k9s.png)

### Installation

Install k9s on a linux machine (e.g. master node):

```bash
$ curl -L https://github.com/derailed/k9s/releases/download/v0.23.10/k9s_Linux_x86_64.tar.gz | tar xz --directory /usr/local/bin k9s
```

Install k9s on mac:

```bash
$ brew install derailed/k9s/k9s
```

For more ways to install please refer to https://github.com/derailed/k9s#installation.

### Usage

Open with:

```bash
$ k9s
```

Keybindings:

K9s uses aliases to navigate most K8s resources.

| Action                                                       | Command                   | Comment                                                      |
| ------------------------------------------------------------ | ------------------------- | ------------------------------------------------------------ |
| Show active keyboard mnemonics and help                      | `?`                       |                                                              |
| Show all available resource alias                            | `ctrl-a`                  |                                                              |
| To bail out of K9s                                           | `:q`, `ctrl-c`            |                                                              |
| **View a Kubernetes resource using singular/plural or short-name** | **`:`po⏎**                | **accepts singular, plural, short-name or alias ie pod or pods** |
| View a Kubernetes resource in a given namespace              | `:`alias namespace⏎       |                                                              |
| Filter out a resource view given a filter                    | `/`filter⏎                | Regex2 supported ie `fred|blee` to filter resources named fred or blee |
| Bails out of view/command/filter mode                        | `<esc>`                   |                                                              |
| **Key mapping to describe, view, edit, view logs,...**       | **`d`,`v`, `e`, `l`,...** |                                                              |
| To view and switch to another Kubernetes namespace           | `:`ns⏎                    |                                                              |
| To delete a resource (TAB and ENTER to confirm)              | `ctrl-d`                  |                                                              |
| To kill a resource (no confirmation dialog!)                 | `ctrl-k`                  |                                                              |

### Tasks

- View Logs of a Grafana Pod
- Start a `exec` session into the Grafana pod
- Scale up the Grafana Deployment

---

## Octant

Octant is a "Highly extensible platform for developers to better understand the complexity of Kubernetes clusters.". Octant is supposed to be used on your local machine (like kubectl or k9s), and uses your kubeconfig to access cluster resources. It is a great tool for visualization of various components. It does a great job in showing dependencies. You can even use octant to edit resources.

* Website: https://octant.dev/
* Github: https://github.com/vmware-tanzu/octant

![](./images/octant.png)

### Installation

MacOS

```
$ brew install octant
```

Linux

```bash
$ curl -L https://github.com/vmware-tanzu/octant/releases/download/v0.16.1/octant_0.16.1_Linux-64bit.tar.gz | tar xz --strip-components=1 --directory /usr/local/bin octant_0.16.1_Linux-64bit/octant
```

Other Binaries can be found on the releases page: https://github.com/vmware-tanzu/octant/releases/tag/v0.16.1.

### Usage

If you put the binary in your part, you can start octant by simply typing `octant`, which should serve the application by default on `http://127.0.0.1:7777`.

```bash
$ octant
```

If you want octant to be served on a specific address, you can adjust it (e.g. to serve on a different port:

```bash
$ octant --listener-addr 127.0.0.1:9999
```

Additional commands, like starting octant with a specific kube context, kubeconfig, etc., can be found in the help menu `octant help`.

### Tasks

- View Logs of a Grafana Pod
- Start a port-forward to the Grafana Service from the Octant UI
- Start a `exec` session into the Grafana pod
- List dependencies of the Grafana Deployment

---

[Back to Index](#index)

---
# Task-1

Please deploy `task1/task.yaml` and see if it works as expected.

```bash
$ kubectl create -f task1/task.yaml
pod/secret-pod created
secret/super-secret created
```

Gosh, something is wrong! Can you fix it without redeploying the pod?

---

[Back to Index](#index)

---

# Task-2

Please deploy `task2/task.yaml`.

```bash
$ kubectl create -f task2/task.yaml
pod/broken-pod created
```

Again something is wrong!  
Can you fix it without changing the pod manifest?

---

[Back to Index](#index)

---

# Task-3

Please deploy `task3/task.yaml`.

```bash
$ kubectl create -f task3/task.yaml
pod/memory-pod created
```

What can you do to successfully start the pod?

---

[Back to Index](#index)

---

# How to recover from a broken cluster

## A Kubernetes worker node has failed

```bash
$ kubectl get nodes
NAME            STATUS     AGE       VERSION
kube-master-1   Ready      21h       v1.18.0
kube-master-2   Ready      20h       v1.18.0
kube-master-3   Ready      20h       v1.18.0
kube-worker-1   Ready      17h       v1.18.0
kube-worker-2   NotReady   17h       v1.18.0
```

### How to recover

1. Kubernetes will automatically reschedule failed pods onto other nodes in the cluster. 
2. Create a new worker to replace the failed node node and join it to the Kubernetes cluster.
3. Once the new worker is working, remove the failed worker:

```bash
$ kubectl delete nodes kube-worker-2
node "kube-worker-2" deleted
```

---

[Back to Index](#index)

---

## A Kubernetes master node has failed

```bash
$ kubectl get nodes
NAME            STATUS     AGE       VERSION
kube-master-1   Ready      21h       v1.18.0
kube-master-2   NotReady   20h       v1.18.0
kube-master-3   Ready      20h       v1.18.0
kube-worker-1   Ready      17h       v1.18.0
kube-worker-2   Ready      17h       v1.18.0
```

### How to recover

1. Create a new Kubernetes node and join it to the working cluster  
   Login to one of the functioning Kubernetes masters and copy the configuration to the new Kubernetes master. Usually this can be accomplished by copying the `/etc/kubernetes`directory from a working master to the new master.
2. Certificates, `kube-apiserver`, `kube-controller-manager`, and `kube-scheduler` configurations should be copied to the new master. If they are defined as systems services, ensure that all services have started properly. If they are run using kubelet manifests in the `/etc/kubernetes/manifests` directory, restart the `kubelet` service and use `docker ps -a` and `docker logs` commands to ensure that the services have started properly.
3. Add the appropriate master labels and taints, either in the kubelet configuration, or by using kubectl:

```bash
$ kubectl label nodes kube-master-4 node-role.kubernetes.io/master=
$ kubectl taint nodes kube-master-4 node-role.kubernetes.io/master=:NoSchedule
```

Once the new master is working, remove the failed master:

```bash
$ kubectl delete nodes kube-master-2
node "kube-master-2" deleted
```

### But my master was not HA

Your Kubernetes cluster is down.

If you still have access to the disk or a snapshot:

1. Attempt to recover the Kubernetes configuration directory containing the original Kubernetes master certificates. Often times this is the `/etc/kubernetes` directory.
2. Assuming that ETCD is still intact, create a new Kubernetes master pointing to the existing ETCD cluster. Use the recovered certificates on the new master. Ensure that `kube-apiserver`, `kube-controller-manager`, and `kube-scheduler` are running on the new master.
3. Login to each worker node and update the `kubelet` configuration to point to the new Kubernetes master. Often times this file is found at `/etc/kubernetes/kubelet.conf`. Restart the `kubelet` service after making this change.

### Bonus: backup your master certificates

Tarball the k8s certificate directory

```bash
$ ls -la /etc/kubernetes/pki/
total 84
drwxr-xr-x 3 root root 4096 Apr 16 08:31 .
drwxr-xr-x 5 root root 4096 Apr 12 13:45 ..
-rw-r--r-- 1 root root 1229 Apr 11 15:23 apiserver.crt
-rw-r--r-- 1 root root 1090 Apr 12 13:45 apiserver-etcd-client.crt
-rw------- 1 root root 1679 Apr 12 13:45 apiserver-etcd-client.key
-rw------- 1 root root 1679 Apr 11 15:23 apiserver.key
-rw-r--r-- 1 root root 1099 Apr 11 15:23 apiserver-kubelet-client.crt
-rw------- 1 root root 1675 Apr 11 15:23 apiserver-kubelet-client.key
-rw-r--r-- 1 root root 1025 Apr 11 15:23 ca.crt
-rw------- 1 root root 1675 Apr 11 15:23 ca.key
-rw-r--r-- 1 root root   17 Apr 16 08:31 ca.srl
drwxr-xr-x 2 root root 4096 Apr 11 15:23 etcd
-rw-r--r-- 1 root root 1038 Apr 11 15:23 front-proxy-ca.crt
-rw------- 1 root root 1675 Apr 11 15:23 front-proxy-ca.key
-rw-r--r-- 1 root root 1058 Apr 11 15:23 front-proxy-client.crt
-rw------- 1 root root 1679 Apr 11 15:23 front-proxy-client.key
-rw------- 1 root root 1675 Apr 11 15:23 sa.key
-rw------- 1 root root  451 Apr 11 15:23 sa.pub
-rw-r--r-- 1 root root 1005 Apr 16 08:31 vt.crt
-rw-r--r-- 1 root root  915 Apr 16 08:30 vt.csr
-rw------- 1 root root 1675 Apr 16 08:30 vt.key

$ tar -zcvf pki-certs.tar.gz /etc/kubernetes/pki
```

---

[Back to Index](#index)

---

## etcd has failed

How to tell, that etcd might have a problem?

```bash
$ kubectl get cs
NAME                 STATUS      MESSAGE
scheduler            Healthy     ok
controller-manager   Healthy     ok
etcd-2               Healthy     {"health": "true"}
etcd-0               Healthy     {"health": "true"}
etcd-1               Unhealthy   Client.Timeout exceeded while awaiting headers
```

### Steps to recover

As Kubernetes is usually configured in HA mode, it is able to survive a failing etcd node. The cluster should still be running.  
The goal should be to recover the failing node, e.g. by providing a new etcd instance.

On one of the working ETCD Nodes, remove the failed ETCD node from the cluster and add the IP Address of the new node:

```bash
$  ETCDCTL_API=3 etcdctl --endpoints=http://127.0.0.1:2379 member list
10b576500ed3ae71: name=kube-etcd-1 peerURLs=https://10.0.0.1:2380 clientURLs=https://10.0.0.1:2379 isLeader=false
30bcb5f2f4c17805: name=kube-etcd-2 peerURLs=https://10.0.0.2:2380 clientURLs=https://10.0.0.2:2379 isLeader=false
a908b0f9f07a7127: name=kube-etcd-3 peerURLs=https://10.0.0.3:2380 clientURLs=https://10.0.0.3:2379 isLeader=true

$ ETCDCTL_API=3 etcdctl --endpoints=http://127.0.0.1:2379 member remove 30bcb5f2f4c17805
Removed member 30bcb5f2f4c17805 from cluster

$ ETCDCTL_API=3 etcdctl member add kube-etcd-4 --peer-urls=http://[new node IP]:2380
Member 2be1eb8f84b7f63e added to cluster ef37ad9dc622a7c4
```

Configure the new ETCD node to connect to the existing cluster:

```bash
export ETCD_NAME="kube-etcd-4"
export ETCD_INITIAL_CLUSTER="kube-etcd-1=https://10.0.0.1:2380,kube-etcd-3=https://10.0.0.3:2380,kube-etcd-4=https://[new node IP]:2380"
export ETCD_INITIAL_CLUSTER_STATE=existing
etcd [flags]
```

Afterwards update every `kube-apiserver` on the master nodes and let `--etcd-servers=` point to the new ETCD node.

### But etcd was not HA

If Kubernetes was not running in HA mode and the only ETCD node has failed, the cluster will be down.

If you still have access to the disk or a snapshot, attempt to recover the ETCD data directory. Typically this is the `/var/lib/etcd` directory. Build a new ETCD node with the recovered data and the same flags as the failed ETCD node except this time, set `ETCD_INITIAL_CLUSTER_STATE=existing`.

Login to the Kubernetes master and update the `kube-apiserver` component’s `--etcd-servers=` option to point to the new etcd node.

### Bonus: how to backup etcd

Take a snapshot of the etcd database

```
sudo ETCDCTL_API=3 etcdctl snapshot save snapshotdb 
--cacert /etc/kubernetes/pki/etcd/server.crt 
--cert /etc/kubernetes/pki/etcd/ca.crt 
--key /etc/kubernetes/pki/etcd/ca.key
```

Verify the snapshot

```
sudo ETCDCTL_API=3 etcdctl --write-out=table snapshot status snapshotdb
+----------+----------+------------+------------+
|   HASH   | REVISION | TOTAL KEYS | TOTAL SIZE |
+----------+----------+------------+------------+
| 45c3ef38 |  1851970 |       1052 |     3.8 MB |
+----------+----------+------------+------------+
```

---

[Back to Index](#index)

---

## I'm not quite sure what broke

### Getting an overview

Check Nodes, are all in `Ready` state?

```bash
$ kubectl get nodes
```

Get status of all cluster components

```bash
$ kubectl get cs
```

Getting information about overall health of your cluster:

```bash
$ kubectl cluster-info dump
```

### Checking Logs

Here are the locations of the relevant log files:

**Master**

* `/var/log/kube-apiserver.log` - API Server, responsible for serving the API
* `/var/log/kube-scheduler.log` - Scheduler, responsible for making scheduling decisions
* `/var/log/kube-controller-manager.log` - Controller that manages replication controllers

**Worker Nodes**

* `/var/log/kubelet.log` - Kubelet, responsible for running containers on the node
* `/var/log/kube-proxy.log` - Kube Proxy, responsible for service load balancing

_Please note:_

* note that on systemd-based systems, you may need to use `journalctl` instead
* also consider logs of your Container Runtime (e.g. Docker)

### Most common root causes

* Operator error, for example misconfigured Kubernetes software or application software
* Data loss or unavailability of persistent storage (e.g. GCE PD or AWS EBS volume)
* VM(s) shutdown
* Network partition within cluster, or between cluster and users
* Crashes in Kubernetes software

Find [here](https://kubernetes.io/docs/tasks/debug-application-cluster/debug-cluster/#specific-scenarios) a list of more specific scenarios.


_Source:_

* https://codefresh.io/Kubernetes-Tutorial/recover-broken-kubernetes-cluster/
* https://kubernetes.io/docs/tasks/debug-application-cluster/debug-cluster/#specific-scenarios

---

[Back to Index](#index)

---