# Best practices

Disclaimer: every recommendation has its exceptions. Make use of what fits your scenario.

## Think about security requirements early

### Traffic encryption

Is traffic encryption required? There are several ways to encrypt the network traffic between nodes and/or pods:

- Overlay network (e.g. Weave Net, Cilium, Calico, ...)
- Service mesh (e.g. Linkerd, Istio, ...)
- other Kubernetes-external encryption mechanisms (IPSec, Wireguard, ...)

### RBAC concept

Implementing a role concept from the beginning can help clarify
responsibilities in the cluster and removes the burden of figuring out who
should be able to do what later on. Integration with a central Single-Sign-On
service via OIDC also removes the need to manage group memberships for each
cluster.

### Security benchmarks

Run security benchmarks, such as the CIS benchmarks for Kubernetes, Docker and
your Linux distribution to find common security problems.

### Runtime security

Tools like Falco can, if properly configured, add another layer of security to
your cluster by monitoring syscalls and alerting when something unexptected
happens in your containers (e.g. a shell is opened).

### Cloud resource access

Do you run applications that need to communicate with the API of your cloud
provider? In that case you might want to install an application that lets you
bind cloud identities (e.g. AWS roles) to your pods. Without this, each pod
would contact the cloud provider API with the same permissions as the node the
pod is running on.

For AWS an example for this is kiam.

### Certificates

Make it easy for applications to use TLS encryption by providing an easy way to
use certificates. The cert-manager can integrate with a variety of backends to
easily create Kubernetes secrets from certificates.

### Be careful what you expose

It is easy to accidently expose an application with a malconfigured Service or
Ingress manifest. Make sure to not expose any non-public services to the
outside. Isolate applications from another using namespaces and NetworkPolicies
before you loose your overview about what communicates with what!

## Cluster structure

### Autoscaling

Do you want your cluster to be able to automatically scale up and down
depending on resource usage? In that case you need to take an extra hard look
at the Kubernetes distribution or installer you plan to use. Cluster
autoscaling on AWS relies on AutoScaling Groups, so any installers that do not
make use of ASGs are probably not autoscalable. Tools that SSH to your nodes to
run the initialization scripts are also a red flag for autoscaling.

For AWS, these distributions/tools support autoscaling out of the box:

- EKS
- Kops

### Namespaces

Use namespaces to separate different applications/teams from another.
Namespaces are the primary mechanism of Kubernetes to restrict access to a
subset of resources. When using the same namespace for multiple
teams/applications, it is difficult to formulate RBAC rules that restrict
access to the minimal set of permissions needed by that team/application.

Namespaces are cheap, use them!

## Applications & Deployment

### Examine added complexities in relation to its usefulness for your case

Creating a high-security Kubernetes cluster that includes runtime security
systems, forwards logs and metrics, is integrated with a SIEM, autoscales
depending on usage to run a Wordpress blog might be overkill.

For each application that you add to your cluster (excluding the business
applications), consider the benefit it brings vs. the complexity it introduces.
Not only business applications need to be operated.

### Use high-quality deployment manifests

Using Helm charts to install applications is easy. However, many Helm charts -
unfortunately - are not very good. Things that are often missing and to look
out for:

- resource requests and limits
- pod security policies
- network policies
- pod disruption budgets
- options to mount additional ConfigMaps/Secrets/Volumes
- Option to customize labels

### Store persistent data outside of Kubernetes

There are differing opinions on the point of running databases on Kubernetes.
If you do choose to run a database on Kubernetes (or any other application that
has a need for a permanent data store), make use of persistent volumes to make
sure that in the event of a desaster, the data survives the cluster.

### Continuous delivery

Use a system that can automatically deploy your applications. This helps to
prevent human errors (e.g. forgetting to set an option) and makes it easy to
create a new cluster with the same applications on it. Suitable tools include
normal pipelines like Jenkins and GitOps tools like ArgoCD and Flux.

## Automation

Automate what you can. The obvious things are deployments, cluster updates,
enforcement of custom policies, backups.

But you can also automate the creation of DNS records for your LoadBalancer
using external-dns, or you can write your own Kubernetes-integrated
applications that automatically interact with your company API when a resource
is changed (status or otherwise) or an event occurs.

Having things automated reduces the operational burden and removes the
possibility for human errors.

## Monitoring, Logging & Alerting

Monitor your applications! Prometheus is a great de-facto standard for
monitoring applications in the Kubernetes ecosystem. Integrate it into your own
business applications to make it even more useful. If long-term storage is
needed, tools like Cortex or Thanos both work very well, depending on the
usecase.

Ship your logs somewhere for long-term storage and/or analysis (if needed).
Logs are often the first point to look at for debugging. Make them available to
developers and make them searchable/filterable.

Common solutions include the ELK/EFK stack or more cloud-native solutions like
Grafana Loki.

Set up some alerts for when things go wrong. But be careful not to overload the
Ops team with alerts. A popular paradigma is to alert only on things that
**really** need human intervention and can not be fixed automatically by
Kubernetes & co.

## Testing

Testing if a Kubernetes cluster works as intended is hard due to its dynamic
nature. Tests that run now might fail tomorrow due to a problem in a component
for which there is no monitoring in place. Tools like sonobuoy and kuberhealthy
can help to create smoke tests for your Kubernetes cluster by
continuously/repeatedly testing different scenarios.

They can be used to augment the monitoring solution already in place for things
that can not be easily monitored.

## Backups

While it is nice to be able to quickly create a new cluster and install all
applications on it automatically by using a CD system, it would be even better
not to have to do that: Backing up your Kubernetes resources and restoring them
if something broke is way faster.

You could either use etcd's snapshot functionality to create backups on an etcd
level, or use tools like Velero, which work on the Kubernetes level.

## Tools

- https://github.com/kubernetes/kops
- https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler
- https://github.com/aquasecurity/kube-bench
- https://github.com/stackrox/kube-linter
- https://github.com/jetstack/cert-manager
- https://github.com/falcosecurity/falco
- https://github.com/argoproj/argo-cd
- https://github.com/fluxcd/flux
- https://github.com/jenkinsci/jenkins
- https://github.com/jenkins-x/jx
- https://github.com/kubernetes-sigs/external-dns
- https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/
- https://github.com/prometheus/prometheus
- https://github.com/thanos-io/thanos
- https://github.com/cortexproject/cortex
- https://github.com/prometheus/alertmanager
- https://github.com/elastic/elasticsearch
- https://github.com/grafana/grafana
- https://github.com/grafana/loki
- https://github.com/vmware-tanzu/sonobuoy
- https://github.com/Comcast/kuberhealthy
- https://github.com/vmware-tanzu/velero
