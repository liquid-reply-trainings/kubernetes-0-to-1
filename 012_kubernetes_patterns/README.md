# Kubernetes Patterns

## Deployment strategies

We have prepared for you the manifests to complete these tasks. You can find
them in the `manifests/deployment_strategies` directory.

The manifests differ in only two significant ways:
1. `default` files do not set the update strategy explicitly, `rolling` files
   use the rolling update strategy and `recreate` files use the recreate update
   strategy.
2. files ending with `-new.yaml` use a different image than the files without
   `-new`, to get Kubernetes to update the deployments.

### Strategy: Rolling Update

To test the rolling update strategy, first create the deployment by running

```
$ kubectl create -f manifests/deployment/strategies/rolling.yaml
```

If you run `kubectl get pods -w` directly after that, your output should look similar to this:

```
$ kubectl get pods -w
NAME                                  READY   STATUS              RESTARTS   AGE
rolling-deployment-6bf8858b74-dwmlg   0/1     ContainerCreating   0          2s
rolling-deployment-6bf8858b74-mhkvh   0/1     ContainerCreating   0          2s
rolling-deployment-6bf8858b74-sdzlm   0/1     ContainerCreating   0          2s
rolling-deployment-6bf8858b74-dwmlg   1/1     Running             0          2s
rolling-deployment-6bf8858b74-sdzlm   1/1     Running             0          3s
rolling-deployment-6bf8858b74-mhkvh   1/1     Running             0          3s
```

Once all pods are in the `Running` status, we can trigger the update by running:

```
$ kubectl apply -f manifests/deployment/strategies/rolling-new.yaml
```

Running `kubectl get pods -w` directly afterwards will produce an output similar to this:

```
$ kubectl get pods -w
NAME                                  READY   STATUS    RESTARTS   AGE
rolling-deployment-6bf8858b74-2snp2   1/1     Running   0          30s
rolling-deployment-6bf8858b74-kgrrh   1/1     Running   0          30s
rolling-deployment-6bf8858b74-n5sdq   1/1     Running   0          28s
rolling-deployment-6bf8858b74-n5sdq   1/1     Terminating   0          28s
rolling-deployment-555d755d55-6vpb2   0/1     Pending       0          0s
rolling-deployment-555d755d55-6vpb2   0/1     Pending       0          0s
rolling-deployment-555d755d55-6vpb2   0/1     ContainerCreating   0          0s
rolling-deployment-555d755d55-7qxj7   0/1     Pending             0          0s
rolling-deployment-555d755d55-7qxj7   0/1     Pending             0          1s
rolling-deployment-555d755d55-7qxj7   0/1     ContainerCreating   0          1s
rolling-deployment-6bf8858b74-n5sdq   0/1     Terminating         0          29s
rolling-deployment-555d755d55-6vpb2   1/1     Running             0          2s
rolling-deployment-555d755d55-7qxj7   1/1     Running             0          2s
rolling-deployment-6bf8858b74-kgrrh   1/1     Terminating         0          32s
rolling-deployment-555d755d55-lxrkh   0/1     Pending             0          0s
rolling-deployment-555d755d55-lxrkh   0/1     Pending             0          0s
rolling-deployment-6bf8858b74-2snp2   1/1     Terminating         0          33s
rolling-deployment-555d755d55-lxrkh   0/1     ContainerCreating   0          1s
rolling-deployment-6bf8858b74-kgrrh   0/1     Terminating         0          33s
rolling-deployment-6bf8858b74-2snp2   0/1     Terminating         0          33s
rolling-deployment-555d755d55-lxrkh   1/1     Running             0          2s
rolling-deployment-6bf8858b74-kgrrh   0/1     Terminating         0          34s
rolling-deployment-6bf8858b74-kgrrh   0/1     Terminating         0          34s
rolling-deployment-6bf8858b74-n5sdq   0/1     Terminating         0          34s
rolling-deployment-6bf8858b74-n5sdq   0/1     Terminating         0          34s
```

As you can see, in the beginning, the old pods are still running. Then, one pod
is being terminated and two new pods are entering the Pending state, then the
ContainerCreating state. The reason for Kubernetes to start two new pods
instead of just one for the one being terminated is that Kubernetes can
temporarily, during a rolling update, create more pods than are wanted - this
can be configured with the `maxSurge` setting. We can also use the
`maxUnavailable` setting to tell Kubernetes how many pods are ok to be
unavailable during the update (in our config, this is set to 1, which is why
you see one pod terminating in the beginning).

Once these new pods are in the Running state, another new pod will be created
in the Pending state and shortly afterwards enters the ContainerCreating state
and then the Running state. The old pods are still in the Terminating state at
the end because Kubernetes needs to clean them up, which can take some time.

The main take-away here is that Kubernetes will successively remove old pods,
start new pods, wait for the new pods to be Running, remove more old pods,
start new pods, wait for the pods to be running, etc.

You can clean up the created deployment by running

```
$ kubectl delete -f manifests/deployment/strategies/rolling-new.yaml
```

### Strategy: Recreate 

Similarly to the last task, we start by creating our deployment:

```
$ kubectl create -f manifests/deployment/strategies/recreate.yaml
```

Running `kubectl get pods -w` directly afterwards, you should get similar output to this:

```
$ kubectl get pods -w
NAME                                   READY   STATUS    RESTARTS   AGE
recreate-deployment-7746bcd8c8-p5rmw   0/1     Pending   0          0s
recreate-deployment-7746bcd8c8-frs6v   0/1     Pending   0          0s
recreate-deployment-7746bcd8c8-m9q6h   0/1     Pending   0          0s
recreate-deployment-7746bcd8c8-m9q6h   0/1     Pending   0          0s
recreate-deployment-7746bcd8c8-p5rmw   0/1     Pending   0          0s
recreate-deployment-7746bcd8c8-frs6v   0/1     ContainerCreating   0          0s
recreate-deployment-7746bcd8c8-m9q6h   0/1     ContainerCreating   0          0s
recreate-deployment-7746bcd8c8-p5rmw   0/1     ContainerCreating   0          0s
recreate-deployment-7746bcd8c8-m9q6h   1/1     Running             0          2s
recreate-deployment-7746bcd8c8-frs6v   1/1     Running             0          2s
recreate-deployment-7746bcd8c8-p5rmw   1/1     Running             0          2s
```

All pods are now created and Running, so we can beginn the update of the deployment:

```
$ kubectl apply -f manifests/deployment/strategies/recreate-new.yaml
```

Running `kubectl get pods -w` again, you should now get output looking like this:

```
$ kubectl get pods -w
NAME                                   READY   STATUS        RESTARTS   AGE
recreate-deployment-7746bcd8c8-p5rmw   1/1     Terminating   0          89s
recreate-deployment-7746bcd8c8-m9q6h   1/1     Terminating   0          89s
recreate-deployment-7746bcd8c8-frs6v   1/1     Terminating   0          89s
recreate-deployment-7746bcd8c8-frs6v   0/1     Terminating   0          90s
recreate-deployment-7746bcd8c8-p5rmw   0/1     Terminating   0          90s
recreate-deployment-7746bcd8c8-m9q6h   0/1     Terminating   0          90s
recreate-deployment-7746bcd8c8-frs6v   0/1     Terminating   0          94s
recreate-deployment-7746bcd8c8-frs6v   0/1     Terminating   0          94s
recreate-deployment-7746bcd8c8-p5rmw   0/1     Terminating   0          104s
recreate-deployment-7746bcd8c8-p5rmw   0/1     Terminating   0          104s
recreate-deployment-7746bcd8c8-m9q6h   0/1     Terminating   0          104s
recreate-deployment-7746bcd8c8-m9q6h   0/1     Terminating   0          104s
recreate-deployment-7bbd9c89ff-482lb   0/1     Pending       0          0s
recreate-deployment-7bbd9c89ff-84cp8   0/1     Pending       0          0s
recreate-deployment-7bbd9c89ff-8jb76   0/1     Pending       0          0s
recreate-deployment-7bbd9c89ff-482lb   0/1     Pending       0          0s
recreate-deployment-7bbd9c89ff-8jb76   0/1     Pending       0          0s
recreate-deployment-7bbd9c89ff-84cp8   0/1     Pending       0          0s
recreate-deployment-7bbd9c89ff-482lb   0/1     ContainerCreating   0          0s
recreate-deployment-7bbd9c89ff-84cp8   0/1     ContainerCreating   0          0s
recreate-deployment-7bbd9c89ff-8jb76   0/1     ContainerCreating   0          1s
recreate-deployment-7bbd9c89ff-8jb76   1/1     Running             0          2s
recreate-deployment-7bbd9c89ff-482lb   1/1     Running             0          3s
recreate-deployment-7bbd9c89ff-84cp8   1/1     Running             0          3s
```

The output here looks very different from the output during the rolling update:
all old pods are being terminated at the same time. Once the old pods are gone,
all three new pods are created simultaniously and go through the different
stages until they are all Running.

From the age of the pods, we can tell that there has been about 16 seconds
where no pod was ready, i.e. we had a downtime of 16 seconds during the update.
This time will vary based on a number of factors (performance of the nodes,
network speed, size of the images being pulled, startup time of the
application).

To clean up this deployment, run

```
$ kubectl delete -f manifests/deployment/strategies/recreate-new.yaml
```

### Default strategy

The default deployment strategy of Kubernetes is the RollingUpdate, so not
setting an update strategy explicitly and updating a deployment will look
similar to our first example.

You can try this out with the provided manifests:

```
$ kubectl create -f manifests/deployment/strategies/default.yaml
$ kubectl get pods -w
$ kubectl apply -f manifests/deployment/strategies/default-new.yaml
$ kubectl get pods -w
$ kubectl delete -f manifests/deployment/strategies/default-new.yaml
```

### Other strategies

Blue/Green and Canary deployment strategies are not supported by Kubernetes out
of the box. There are some 3rd party projects that still allow you to
accomplish this (usually implemented by using CRDs):

- [Argo Rollouts](https://argoproj.github.io/argo-rollouts/)
- [Flagger](https://flagger.app/)
- [Istio](https://istio.io/latest/docs/concepts/traffic-management/)

## DaemonSet

DaemonSets can be used to make sure a certain application is running on all (or
a subset of) nodes of the cluster. An example DaemonSet that prints out the
hostname of the node in the logs can be found in
`manifests/daemonset/daemonset.yaml`.

You can create the DaemonSet with the usual command:

```
$ kubectl create -f manifests/daemonset/daemonset.yaml
```

You should now be able to see (by running `kubectl get pods -w`) that one pod for each node is starting.

Once the pods are started, you can get the hostname of the node each pod is
running on by inspecting the logs of each pod. You can look at the
daemonset.yaml to see how this was accomplished.

Clean up the DaemonSet by running

```
$ kubectl delete -f manifests/daemonset/daemonset.yaml
```

## Self Awareness

There are easier (and safer) ways for a pod to get the name of the node it is
running on (or even information about its own configuration). In
`manifests/self-awareness/daemonset.yaml` you can find a DaemonSet doing a
similar thing as in the last task, only this time without mounting the
`/etc/hostname` file from the node.

Install the DaemonSet by running

```
$ kubectl create -f manifests/self-awareness/daemonset.yaml
```

You can again inspect the logs of the pods to see the name of the node each pod
is running on.

NOTE: this **can** differ from the name you got in the last task. The reason
for that is that in this task you get the name the node used to register itself
with the Kubernetes control plane. In the last task you got the hostname as it
is configured on the Linux system. Depending on the configuration of the
Kubelet, you might get a longer or shorter form of the Linux hostname (or even
something completely different) - depending on your use-case, make sure you get
the right value.

Clean up the deployed resources by running

```
$ kubectl delete -f manifests/self-awareness/daemonset.yaml
```

## Init Container

Init containers can be used to run code before the actual application starts.
In our task, we use the init container to retrieve the search result for
"radiohead" on iTunes. It will save the result in a volume that is shared with
the main application. The main application then uses the file to extract some
information about the search results and print them to stdout.

Inspect the file `manifests/init-container/deployment.yaml` to make sure you
understand how this works.

Create the deployment by running

```
$ kubectl create -f manifests/init-container/deployment.yaml
```

Inspect the logs of the pod, once it is running. You will see the returned search results.

Clean up the deployment with

```
$ kubectl delete -f manifests/init-container/deployment.yaml
```

Can you think of possible real-world applications of this pattern?

## Sidecar Container

The example in this task is a bit more complicated, to show off some of the
features of sidecar containers:
We have 4 containers running in the same pod at the same time.
1. the first container, in a loop, prints the current date to the end of a file in a
   shared volume
2. the second container is running nginx to serve the file from the shared
   volume via HTTP
3. the third container, in a loop, requests the file via HTTP from `localhost`
   and writes the length of the file into another file in another shared volume
4. the last container, in a loop, prints the content from the file in the
   second shared volume to stdout (with some extra text)

As you can see here, sidecar containers can make use of a variety of features:
1. shared volumes, like with init-containers
2. all containers in a pod share a network namespace, which means they can
   communicate via `localhost`
3. containers can augment one another with additional features (like serving a
   file via HTTP)

Try out the example by creating the deployment:

```
$ kubectl create -f manifests/sidecar-container/deployment.yaml
```

You can now use `kubectl logs -f <pod_name> -c printer` to follow the output of the fourth container.


Clean up the resources with

```
$ kubectl delete -f manifests/sidecar-container/deployment.yaml
```

Can you think of scenarios where this pattern is useful? Hint: Think of e.g.
proxies, authentication, translating between data formats, etc

## Horizontal Pod Autoscaler


Pods doesn't scale per default. You will have to set a HPA to specify the required thresholds. 

For this case we are going to apply a new application:

```yaml
kubectl apply -f manifests/hpa/php-apache.yaml
```

Now we can configure the HPA for the apache server and check if it is correctly applied.

```
kubectl autoscale deployment php-apache --cpu-percent=50 --min=1 --max=10
kubectl get hpa
```

Time to test the HPA, with some simple load generation
```
kubectl run -i \
    --tty load-generator \
    --rm --image=busybox \
    --restart=Never \
    -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache; done"
```

Open a new shell tab, to see how the hpa & deployment is doing
```
kubectl get hpa php-apache
kubectl get deployment php-apache
kubectl get po
```

Now stop the load generator and double check the hpa.





