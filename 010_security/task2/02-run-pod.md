# Good Pods are not running as root
Try to run a Pod as root:
```
cat nginx-privileged.yaml
kubectl apply -f nginx-privileged.yaml
```

Why is this not working? Try to adapt the kuard-privileged.yaml to run the Pod.

Solution:
```
cat nginx-restricted.yaml
kubectl apply -f nginx-restricted.yaml
```

