# Test Implemented Roles

Test the rights of your service account:
```
kubectl --as=system:serviceaccount:<yournamespace>:rbac-test-sa auth can-i get pods
```
You should receive a Yes.

Now let's see if we can read secrets:
```
kubectl --as=system:serviceaccount:<yournamespace>:rbac-test-sa auth can-i get secrets
```

## What happens if you run the last command without namespace?

## What happen if you run only the `kubectl auth can-i get secrets`?