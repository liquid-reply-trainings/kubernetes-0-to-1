# Role Base Access Control
##  Setup a Role within a namespace

Implement the following:
* Create the service account “rbac-test-sa” 
* Create a role “rbac-test-role” that grants the following pod level resources:
  * Get
  * Watch
  * List
* Bind the “rbac-test-sa” service account to the “rbac-test-role” role
```
kubectl apply -f roles/role.yaml
```

* Test RBAC is working by trying to do something the service account is not authorised to do


<!-- ## Setup a Cluster Role

Implement the following:
* Create the user “cluster-user-secretadmin” authenticating with a password
```
kubectl config set-credentials cluster-user-secretadmin --username=cluster-user-secretadmin --password=supersecret
```
* Create a cluster role “cluster-role-secretadmin” that grants the following cluster level secret resources:
  * Get
  * Watch
  * List
* Bind “cluster-user-secretadmin” user to the “cluster-role-secretadmin”
* How you will test that the rights are correct? -->